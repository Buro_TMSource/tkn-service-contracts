package com.teknei.bid.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class ContractExstreamService {

    @Value("${tkn.exstream.outputXMLFile}")
    private String outputURL;
    @Value("${tkn.exstream.outputPDFFile}")
    private String outputPDFURL;
    @Value("${tkn.exstream.command}")
    private String commandExstream;
    private static final Logger log = LoggerFactory.getLogger(ContractExstreamService.class);
    private static final SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy");

    public byte[] generateContract(String name, String lastname, String curp, String documentNumber, String address, String date, String mail, String cel, String tel) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".generateContract 1 ");
    	StringBuffer buff = new StringBuffer();
        buff.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
                .append("<document>\n")
                .append("<NUMSUC>6994</NUMSUC>\n")
                .append("<NOMSUC>SUCURSAL MIRAFLORES</NOMSUC>\n")
                .append("<GVPBRP1>W0998</GVPBRP1>\n")
                .append("<GVPBRP2>W0998</GVPBRP2>\n")
                .append("<OAAOCIS>95556</OAAOCIS>\n")
                .append("<OAACNUM>6300146159</OAACNUM>\n")
                .append("<OAACMNR>OAACMNR</OAACMNR>\n")
                .append("<PGZCODE>17</PGZCODE>\n")
                .append("<PGZDESC>Cuenta Flexible Simple HSBC</PGZDESC>\n")
                .append("<OAAOTF>Individual</OAAOTF>\n")
                .append("<OAACFEAL>24/10/2016</OAACFEAL>\n")
                .append("<CLABE>021320063001461593</CLABE>\n")
                .append("<LUGYFECH>MEXICO D.F.,").append(sdf.format(new Date())).append("</LUGYFECH>\n")
                .append("<NOMCLIEN>" + name + " " + lastname + "</NOMCLIEN>\n")
                .append("<OAAOSXTX>Femenino</OAAOSXTX>\n")
                .append("<NOMMNR>NOMMNR</NOMMNR>\n")
                .append("<OAACURP>" + curp + "</OAACURP>\n")
                .append("<OAAPAISI1>MEXICO</OAAPAISI1>\n")
                .append("<OAAPAISI2>                   </OAAPAISI2>\n")
                .append("<OAAPAISI3>                   </OAAPAISI3>\n")
                .append("<OAAORFC>" + documentNumber + "</OAAORFC>\n")
                .append("<OAAORFC2>                  </OAAORFC2>\n")
                .append("<OAAORFC3>                  </OAAORFC3>\n")
                .append("<OAAONTTX>MEXICO</OAAONTTX>\n")
                .append("<OAAONTTX1>                      </OAAONTTX1>\n")
                .append("<OAAONTTX2>                      </OAAONTTX2>\n")
                .append("<OAAPAISNC>MEXICO</OAAPAISNC>\n")
                .append("<OAAOSATX>Soltero</OAAOSATX>\n")
                .append("<OAAODOB>07/06/1978</OAAODOB>\n")
                .append("<ENFENACI>CDMX</ENFENACI>\n")
                .append("<OAAOSTE1>" + address + "</OAAOSTE1>\n")
                .append("<OAAPAISNC>MEXICO</OAAPAISNC>\n")
                .append("<OAAFIEL>OAAFIEL</OAAFIEL>\n")
                .append("<OAAOSTE2>OAAOSTE2</OAAOSTE2>\n")
                .append("<OAAOPHONM>" + cel + "</OAAOPHONM>\n")
                .append("<PRNYAPHN>" + tel + "</PRNYAPHN>\n")
                .append("<OAAOEML>" + mail + "</OAAOEML>\n")
                .append("<PRAOBKSG>Comercial</PRAOBKSG>\n")
                .append("<PRAOECSE>Particulares</PRAOECSE>\n")
                .append("<OAAOPCJ>7</OAAOPCJ>\n")
                .append("<RNOTIN>RNOTIN</RNOTIN>\n")
                .append("<!-- Informacion Laboral -->\n")
                .append("<PRNYACON>HSBC</PRNYACON>\n")
                .append("<OAAOPHOF>5557213390</OAAOPHOF>\n")
                .append("<PROFSTE1>C. REFORMA NO. 156 COL. JUAREZ 06600 CUAUHTEMOC, D.F.</PROFSTE1>\n")
                .append("<PRNYACO3> </PRNYACO3>\n")
                .append("<PRNYACO1> </PRNYACO1>\n")
                .append("<PRNYACO2> </PRNYACO2>\n")
                .append("<OAAOATTX>EDUCADORA</OAAOATTX>\n")
                .append("<PRNYATA1>SUELDOS</PRNYATA1>\n")
                .append("<PRNYATA2>$ DE $4,001 A $10,000</PRNYATA2>\n")
                .append("<PRNYAAGE>Servicios</PRNYAAGE>\n")
                .append("<PRNYAPOS>ADMINISTRADOR DE SISTEMAS</PRNYAPOS>\n")
                .append("<PRNYAAES>SALON DE FIESTAS</PRNYAAES>\n")
                .append("<!-- Perfil Transaccional -->\n")
                .append("<PRNYAAT1>PRNYAAT1</PRNYAAT1>\n")
                .append("<PRNYAAT3>PRNYAAT3</PRNYAAT3>\n")
                .append("<ENUMDEP>ENUMDEP</ENUMDEP>\n")
                .append("<CNUMDEP>CNUMDEP</CNUMDEP>\n")
                .append("<SPNUMDEP>SPNUMDEP</SPNUMDEP>\n")
                .append("<TNUMDEP>TNUMDEP</TNUMDEP>\n")
                .append("<EMONDEP>EMONDEP</EMONDEP>\n")
                .append("<CMONDEP>CMONDEP</CMONDEP>\n")
                .append("<SPMONDEP>SPMONDEP</SPMONDEP>\n")
                .append("<TMONDEP>TMONDEP</TMONDEP>\n")
                .append("<ENUMRET>ENUMRET</ENUMRET>\n")
                .append("<CNUMRET>CNUMRET</CNUMRET>\n")
                .append("<SPNUMRET>SPNUMRET</SPNUMRET>\n")
                .append("<TNUMRET>TNUMRET</TNUMRET>\n")
                .append("<EMONRET>EMONRET</EMONRET>\n")
                .append("<CMONRET>CMONRET</CMONRET>\n")
                .append("<SPMONRET>SPMONRET</SPMONRET>\n")
                .append("<TMONRET>TMONRET</TMONRET>\n")
                .append("<DPAIS>DPAIS</DPAIS>\n")
                .append("<RPAIS>RPAIS</RPAIS>\n")
                .append("<PRNYABR1> </PRNYABR1>\n")
                .append("<PRNYABR2> </PRNYABR2>\n")
                .append("<PRNYABR3> </PRNYABR3>\n")
                .append("<PRNYABR4> </PRNYABR4>\n")
                .append("<PRNYANIN>PRNYANIN</PRNYANIN>\n")
                .append("<PRNYANCT>PRNYANCT</PRNYANCT>\n")
                .append("<PRNYATER>PRNYATER</PRNYATER>\n")
                .append("<PRNYATID>PRNYATID</PRNYATID>\n")
                .append("<PRNYANDU>PRNYANDU</PRNYANDU>\n")
                .append("<PRNYATID>PRNYATID</PRNYATID>\n")
                .append("<PRNYAIDN>PRNYAIDN</PRNYAIDN>\n")
                .append("<PRNYACDE>PRNYACDE</PRNYACDE>\n")
                .append("<!--  Pie de pagina -->\n")
                .append("<LEYEN1>EL CLIENTE MANIFIESTA QUE AL MOMENTO DE LA SOLICITUD Y CONTRATACIÓN, NO CONOCE LA HOMOCLAVE DE SU RFC</LEYEN1>\n")
                .append("<LEYEN2>EL CLIENTE MANIFIESTA QUE AL MOMENTO DE LA SOLICITUD Y CONTRATACIÓN, NO CUENTA Y/O DISPONE CON C.U.R.P.</LEYEN2>\n")
                .append("<RECA>0310-003-/01-06762-101904401</RECA>\n")
                .append("<FECHA>15/10/2014</FECHA>\n")
                .append("<PGRMPRSN>REIMPRESION</PGRMPRSN>\n")
                .append("<!-- Cabecera de la segunda página -->\n")
                .append("<REGCONT>0310-003-/01-06762-1019044014</REGCONT>\n")
                .append("<PRTPFR1>INDIVIDUAL</PRTPFR1>\n")
                .append("<PRNMFR1>MARIA MARIBEL GUADALUPE SALINAS ESPINOSA</PRNMFR1>\n")
                .append("<PRTPFR2>INDIVIDUAL</PRTPFR2>\n")
                .append("<PRNMFR2>FABIOLA CHU HERNANDEZ</PRNMFR2>\n")
                .append("<GVPBRP2C>W099</GVPBRP2C>\n")
                .append("<PRNYAEMP>0440784</PRNYAEMP>\n")
                .append("<DD>2</DD>\n")
                .append("<MM>5</MM>\n")
                .append("<YY>17</YY>\n")
                .append("<OACOMEN1>MANCOMUNADA</OACOMEN1>\n")
                .append("<CONU></CONU>\n")
                .append("<CONI></CONI>\n")
                .append("<CONM> </CONM>\n")
                .append("<MOVN></MOVN>\n")
                .append("<MOVS> </MOVS>\n")
                .append("<PRDAUTY></PRDAUTY>\n")
                .append("<PRDAUTN> </PRDAUTN>\n")
                .append("<NOMTITULAR>MARIA MARIBEL GUADALUPE SALINAS</NOMTITULAR>\n")
                .append("</document>\n")
        ;
        String url = outputURL.trim().replace("#", "");
        String pdfUrl = outputPDFURL.trim().replace("#", "");
        try {
            Path xmlPath = Paths.get(url);
            if (Files.exists(xmlPath)) {
                Files.delete(xmlPath);
            }
            Files.write(Paths.get(url), buff.toString().getBytes(), StandardOpenOption.CREATE);
            Process p = Runtime.getRuntime().exec(commandExstream);
            p.waitFor();
            Path pdfPath = Paths.get(pdfUrl);
            if (Files.exists(pdfPath)) {
                byte[] pdfBytes = Files.readAllBytes(pdfPath);
                return pdfBytes;
            }
        } catch (IOException | InterruptedException e) {
            log.error("Error in generating PDF file: {}", e.getMessage());
        }
        return null;
    }

}
