package com.teknei.bid.service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.teknei.bid.persistence.entities.BidClieCurp;
import com.teknei.bid.persistence.entities.BidClieRegProc;
import com.teknei.bid.persistence.entities.BidClieRegProcPK;
import com.teknei.bid.persistence.repository.BidCurpRepository;
import com.teknei.bid.persistence.repository.BidRegProcRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class ContractDatabaseService {
	private static final Logger log = LoggerFactory.getLogger(ContractDatabaseService.class);
    @Autowired
    private BidCurpRepository bidCurpRepository;
    @Autowired
    private BidRegProcRepository bidRegProcRepository;

    public void alterRecord(Long operationId, boolean created, String username) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".alterRecord ");
        BidClieCurp curp = bidCurpRepository.findTopByIdClie(operationId);
        BidClieRegProcPK pk = new BidClieRegProcPK();
        pk.setIdClie(operationId);
        pk.setCurp(curp.getCurp());
        BidClieRegProc proc = bidRegProcRepository.findOne(pk);
        proc.setRegCont(created);
        proc.setUsrCrea(username);
        proc.setUsrOpeCrea(username);
        proc.setIdEsta(1);
        proc.setIdTipo(3);
        proc.setFchCrea(new Timestamp(System.currentTimeMillis()));
        proc.setFchRegCont(new Timestamp(System.currentTimeMillis()));
        bidRegProcRepository.save(proc);
    }

}
