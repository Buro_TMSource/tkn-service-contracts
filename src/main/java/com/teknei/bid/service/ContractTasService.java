package com.teknei.bid.service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.teknei.bid.dto.PersonData;
import com.teknei.bid.persistence.entities.BidClie;
import com.teknei.bid.persistence.entities.BidScan;
import com.teknei.bid.persistence.entities.BidClieTas;
import com.teknei.bid.persistence.repository.BidClieRepository;
import com.teknei.bid.persistence.repository.BidScanRepository;
import com.teknei.bid.persistence.repository.BidTasRepository;
import com.teknei.bid.util.tas.TasManager;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Service
public class ContractTasService {
	private static final Logger log = LoggerFactory.getLogger(ContractTasService.class);
    @Autowired
    private BidScanRepository bidScanRepository;
    @Autowired
    private BidTasRepository bidTasRepository;
    @Value("${tkn.tas.name}")
    private String tasName;
    @Value("${tkn.tas.surname}")
    private String tasSurname;
    @Value("${tkn.tas.lastname}")
    private String tasLastname;
    @Value("${tkn.tas.identification}")
    private String tasIdentification;
    @Value("${tkn.tas.date}")
    private String tasDate;
    @Value("${tkn.tas.scan}")
    private String tasScan;
    @Value("${tkn.tas.id}")
    private String tasOperationId;
    @Value("${tkn.tas.casefile}")
    private String tasCasefile;
    @Value("${tkn.tas.idType}")
    private String tasTypeId;
    @Value("${tkn.tas.contract}")
    private String tasContract;
    @Value("${tkn.tas.signedContract}")
    private String tasContractSigned;
    @Value("${tkn.tas.reference}")
    private String tasReference;
    @Value("${tkn.tas.contractPrefilled}")
    private String tasContractPrefilled;
    @Value("${tkn.tas.nom151}")
    private String tasNom151;
    @Autowired
    private TasManager tasManager;
    @Autowired
    private BidClieRepository clieRepository;

    public void addContractToTas(byte[] contract, Long id) throws Exception 
    {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".addContractToTas ");
        BidScan bidScan = bidScanRepository.findByIdRegi(id);
        BidClieTas bidTas = bidTasRepository.findByIdClie(id);
        Map<String, String> docProperties = getMetadataMapAddress(bidScan.getScanId(), id);
        String docNumber = docProperties.get(tasIdentification);
        tasManager.addDocument(tasContract, bidTas.getIdTas(), null, docProperties, contract, "application/pdf", "Contrato-" + docNumber + ".pdf");
    }

    public void addContactSignedToTas(byte[] contract, Long id) throws Exception 
    {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".addContactSignedToTas");
        BidScan bidScan = bidScanRepository.findByIdRegi(id);
        BidClieTas bidTas = bidTasRepository.findByIdClie(id);
        Map<String, String> docProperties = getMetadataMapAddress(bidScan.getScanId(), id);
        String docNumber = docProperties.get(tasIdentification);
        tasManager.addDocument(tasContractSigned, bidTas.getIdTas(), null, docProperties, contract, "application/pdf", "Contrato-signed" + docNumber + ".pdf");
    }

    public void addContactPrefilledWithCertToTas(byte[] contract, Long id) throws Exception 
    {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".addContactPrefilledWithCertToTas ");
        BidScan bidScan = bidScanRepository.findByIdRegi(id);
        BidClieTas bidTas = bidTasRepository.findByIdClie(id);
        Map<String, String> docProperties = getMetadataMapAddress(bidScan.getScanId(), id);
        String docNumber = docProperties.get(tasIdentification);
        tasManager.addDocument(tasContractPrefilled, bidTas.getIdTas(), null, docProperties, contract, "application/pdf", "Contrato-prefilled" + docNumber + ".pdf");
    }

    public void addContactNom151EvidencetToTas(byte[] contract, Long id) throws Exception 
    {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".addContactNom151EvidencetToTas ");
        BidScan bidScan = bidScanRepository.findByIdRegi(id);
        BidClieTas bidTas = bidTasRepository.findByIdClie(id);
        Map<String, String> docProperties = getMetadataMapAddress(bidScan.getScanId(), id);
        String docNumber = docProperties.get(tasIdentification);
        tasManager.addDocument(tasNom151, bidTas.getIdTas(), null, docProperties, contract, "application/pdf", "Contrato-" + docNumber + ".pdf");
    }

    private Map<String, String> getMetadataMapAddress(String scanId, Long operationId) throws Exception 
    {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getMetadataMapAddress ");
        PersonData scanInfo = getPersonalDataFromScan(scanId, operationId);
        Map<String, String> docProperties = new HashMap<>();
        docProperties.put(tasName, scanInfo.getName());
        docProperties.put(tasSurname, scanInfo.getSurename());
        docProperties.put(tasLastname, scanInfo.getSurenameLast());
        docProperties.put(tasIdentification, scanInfo.getPersonalNumber());
        docProperties.put(tasReference, scanInfo.getPersonalNumber());
        String dateISO8601 = ZonedDateTime.now().format(DateTimeFormatter.ISO_INSTANT).toString();
        docProperties.put(tasDate, dateISO8601);
        return docProperties;
    }

    public PersonData getPersonalDataFromScan(String scanId, Long operationId) throws Exception 
    {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getPersonalDataFromScan ");
        PersonData personalData = new PersonData();
        BidClie bidClie = clieRepository.findOne(operationId);
        personalData.setPersonalNumber(String.valueOf(operationId));
        String name = bidClie.getNomClie() == null ? "" : bidClie.getNomClie();
        String surname = bidClie.getApePate() == null ? "" : bidClie.getApePate();
        String surnamelast = bidClie.getApeMate() == null ? "" : bidClie.getApeMate();
        String surnames = surname + " " + surnamelast;
        personalData.setName(name);
        personalData.setSurename(surname);
        personalData.setSurenameLast(surnamelast);
        personalData.setLastNames(surnames);
        return personalData;
    }

}
