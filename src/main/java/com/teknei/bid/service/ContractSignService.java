package com.teknei.bid.service;

import com.teknei.bid.dto.SignContractRequestDTO;
import com.teknei.bid.persistence.entities.BidClieFirm;
import com.teknei.bid.persistence.entities.BidTipoFirm;
import com.teknei.bid.persistence.repository.BidClieFirmRepository;
import com.teknei.bid.persistence.repository.BidTipoFirmRepository;
import com.teknei.bid.util.tas.TasManager;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.jnbis.api.Jnbis;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Service
public class ContractSignService {

    private static final Logger log = LoggerFactory.getLogger(ContractSignService.class);

    @Autowired
    private TasManager tasManager;
    @Value("${tkn.contract.signed.tempPath}")
    private String tempFolder;
    @Value("${tkn.contract.signed.javaComponent}")
    private String uriJavaComponentLocation;
    @Value("${tkn.contract.signed.javaFullPath}")
    private String fullJavaLocation;
    @Autowired
    private ThumbService thumbService;
    @Value("${tkn.contract.sign.coords}")
    private String tknSignCoords;
    @Autowired
    private BidTipoFirmRepository tipoFirmRepository;
    @Autowired
    private BidClieFirmRepository bidClieFirmRepository;
    @Value("${tkn.contract.signed.colorScript}")
    private String scriptColorImage;
    private static final String STATUS_ACCEPTANCE_FIRST = "AUTO_INFO_CRED";
    private static final String STATUS_ACCEPTANCE_SECOND = "AUTO_COMP_INFO";
    private static final String STATUS_ACCEPTANCE_THIRD = "AUTO_FINE_MERC";
    private BidTipoFirm firmFirst;
    private BidTipoFirm firmSecond;
    private BidTipoFirm firmThird;

    @PostConstruct
    private void init() {
        initTipoFirm();
    }

    private void initTipoFirm() 
    {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+". ");
        try {
            firmFirst = tipoFirmRepository.findByCodTipoFirmAndIdEsta(STATUS_ACCEPTANCE_FIRST, 1);
            firmSecond = tipoFirmRepository.findByCodTipoFirmAndIdEsta(STATUS_ACCEPTANCE_SECOND, 1);
            firmThird = tipoFirmRepository.findByCodTipoFirmAndIdEsta(STATUS_ACCEPTANCE_THIRD, 1);
        } catch (Exception e) {
            log.error("Error looking for correct ids for acceptance with message: {}", e.getMessage());
            firmFirst = new BidTipoFirm();
            firmSecond = new BidTipoFirm();
            firmThird = new BidTipoFirm();
            firmFirst.setIdTipoFirm(7l);
            firmSecond.setIdTipoFirm(8l);
            firmThird.setIdTipoFirm(9l);
        }
    }


    public byte[] signContract(SignContractRequestDTO contractRequestDTO) throws Exception 
    {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+". ");
        String uriJavaComponent = uriJavaComponentLocation.trim().replace("#", "");
        String javaUri = fullJavaLocation.trim().replace("#", "");
        String url = tempFolder.trim().replace("#", "");
        String fileContractOutputUrl = new StringBuilder(url).append("/").append(contractRequestDTO.getOperationId()).append(".pdf").toString();
        String fileContractSignedOutputUrl = new StringBuilder(url).append("/").append(contractRequestDTO.getOperationId()).append("_signed.pdf").toString();
        String fileFingerprintBinaryOutputUrl = new StringBuilder(url).append("/").append(contractRequestDTO.getOperationId()).append("binaryFingerprint.bin").toString();
        String fileFingerprintImageOutputUrl = new StringBuilder(url).append("/").append(contractRequestDTO.getOperationId()).append("imageFingerprint.jpeg").toString();
        byte[] contract = tasManager.getContract(String.valueOf(contractRequestDTO.getOperationId()));
        try{
            byte[] contractWithCertData = tasManager.getContractFilled(String.valueOf(contractRequestDTO.getOperationId()));
            if(contractWithCertData != null){
                log.info("Contract with cert data found, assigning");
                contract = contractWithCertData;
            }
        }catch (Exception e){
            log.info("Error getting contract with cert data: {}", e.getMessage());
        }
        if (contract == null) {
            log.warn("No contract found for: {}", contractRequestDTO.getOperationId());
            return null;
        }
        String contentType = contractRequestDTO.getContentType();
        byte[] encoded = Base64Utils.decodeFromString(contractRequestDTO.getBase64Finger());
        byte[] imageBytes = null;
        if (contentType.toLowerCase().contains("wsq")) {
            try {
                imageBytes = Jnbis.wsq().decode(encoded).toJpg().asByteArray();
            } catch (Exception e) {
                log.error("Error extracting image from wsq content, message: {}", e.getMessage());
                log.warn("Using raw information");
                imageBytes = encoded;
            }
        } else {
            log.info("No wsq content found in ws request, found: {}", contentType);
            imageBytes = encoded;
        }
        Files.write(Paths.get(fileContractOutputUrl), contract);
        Files.write(Paths.get(fileFingerprintBinaryOutputUrl), encoded);
        Files.write(Paths.get(fileFingerprintImageOutputUrl), imageBytes);
        String fileUriThumb = fileFingerprintImageOutputUrl.replace("imageFingerprint", "imageFingerprintThumb");
        thumbService.generateThumb(fileFingerprintImageOutputUrl, fileUriThumb);
        String fileUriThumbColor = fileUriThumb.replace("imageFingerprintThumb", "imageFingerprintThumbColor");
        fileUriThumbColor = fileUriThumbColor.replace(".jpeg", ".png");
        StringBuilder commandColor = new StringBuilder("python ").append(scriptColorImage).append(" ")
                .append(fileUriThumb).append(" ")
                .append(fileUriThumbColor);
        log.info("Executing: {}", commandColor.toString());
        try {
            CommandLine cmdLine = CommandLine.parse(commandColor.toString());
            DefaultExecutor defaultExecutor = new DefaultExecutor();
            defaultExecutor.execute(cmdLine);
            Thread.sleep(500);
        } catch (Exception e) {
            log.error("Error coloring image with message: {}", e.getMessage());
            log.error("Trace: ", e);
            fileUriThumbColor = fileUriThumb;
        }
        /*
        *
        * CommandLine cmdLine = CommandLine.parse(s);
            DefaultExecutor executor = new DefaultExecutor();
            executor.execute(cmdLine);

            */


        String[] coords = tknSignCoords.split("\\|");
        /*
        Coords in [1-3] are optional
        * */
        List<String> finalCoords = new ArrayList<>();
        finalCoords.add(coords[0]);
        BidClieFirm clieFirmFirst = bidClieFirmRepository.findByIdClieAndIdTipoFirm(contractRequestDTO.getOperationId(), firmFirst.getIdTipoFirm());
        if (clieFirmFirst == null || clieFirmFirst.getBolAcep() == null || clieFirmFirst.getBolAcep() == false) {
            log.info("Not accepted condition, skipping firm");
        } else {
            finalCoords.add(coords[1]);
        }
        BidClieFirm clieFirmSecond = bidClieFirmRepository.findByIdClieAndIdTipoFirm(contractRequestDTO.getOperationId(), firmSecond.getIdTipoFirm());
        if (clieFirmSecond == null || clieFirmSecond.getBolAcep() == null || clieFirmSecond.getBolAcep() == false) {
            log.info("Not accepted condition, skipping firm");
        } else {
            finalCoords.add(coords[2]);
        }
        BidClieFirm clieFirmThird = bidClieFirmRepository.findByIdClieAndIdTipoFirm(contractRequestDTO.getOperationId(), firmThird.getIdTipoFirm());
        if (clieFirmThird == null || clieFirmThird.getBolAcep() == null || clieFirmThird.getBolAcep() == false) {
            log.info("Not accepted condition, skipping firm");
        } else {
            finalCoords.add(coords[3]);
        }
        if (coords.length > 6) {
            finalCoords.add(coords[4]);
            finalCoords.add(coords[5]);
            finalCoords.add(coords[6]);
        	
        }
        List<String> commandList = new ArrayList<>();
        String commandFirst = new StringBuilder()
        		.append(javaUri)
                .append(" -jar ")
                .append(uriJavaComponent)
                .append(" ")
                .append("\"")
                .append(fileContractOutputUrl)
                .append("\"")
                .append(" ")
                .append("\"")
                .append(fileContractSignedOutputUrl)
                .append("\"")
                .append(" ")
                .append(fileFingerprintBinaryOutputUrl)
                .append(" ")
                .append(fileUriThumbColor)
                .append(" ")
                .append(coords[0])
                .toString();
        commandList.add(commandFirst);
        String target = "";
        List<String> docsList = new ArrayList<>();
        //log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".  >>>  commandFirst>> "+commandFirst);
        docsList.add(fileContractSignedOutputUrl);
        if (commandFirst.length() > 1) {
            String source = fileContractSignedOutputUrl;
            Integer tryNo = 1;
            for (String s : finalCoords) {
                target = fileContractSignedOutputUrl + "_" + tryNo;
                String command = new StringBuilder()
                        .append(javaUri)
                        .append(" -jar ")
                        .append(uriJavaComponent)
                        .append(" ")
                        .append("\"")
                        .append(source)
                        .append("\"")
                        .append(" ")
                        .append("\"")
                        .append(target)
                        .append("\"")
                        .append(" ")
                        .append(fileFingerprintBinaryOutputUrl)
                        .append(" ")
                        .append(fileUriThumbColor)
                        .append(" ")
                        .append(s)
                        .toString();
                //log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".  >>>  command>> "+command);
                commandList.add(command);
                docsList.add(target);
                tryNo++;
                source = target;
            }
        }
        for (String s : commandList) {
            log.info("Executing: {}", s);
            CommandLine cmdLine = CommandLine.parse(s);
            DefaultExecutor executor = new DefaultExecutor();
            executor.execute(cmdLine);
            Thread.sleep(3000);
        }
        byte[] signedContractBytes = null;
        if (target == null || target.isEmpty()) {
            signedContractBytes = Files.readAllBytes(Paths.get(fileContractSignedOutputUrl));
        } else {
            signedContractBytes = Files.readAllBytes(Paths.get(target));
        }
        if (!docsList.isEmpty()) {
            docsList.forEach(d -> {
                try {
                    Files.delete(Paths.get(d));
                } catch (IOException e) {
                    log.error("Unable to delete doc: {} with message: {}", d, e.getMessage());
                }
            });

        }
        /*
        try {
            Files.delete(Paths.get(fileUriThumbColor));
            Files.delete(Paths.get(fileContractOutputUrl));
            Files.delete(Paths.get(fileContractSignedOutputUrl));
            Files.delete(Paths.get(fileFingerprintBinaryOutputUrl));
            Files.delete(Paths.get(fileFingerprintImageOutputUrl));
            Files.delete(Paths.get(fileUriThumb));
        } catch (Exception e) {
            log.error("Error deleting file with message: {}", e.getMessage());
        }*/
        return signedContractBytes;
    }
}
