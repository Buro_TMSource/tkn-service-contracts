package com.teknei.bid.service;

import com.teknei.bid.controller.rest.crypto.Decrypt;
import com.teknei.bid.dto.CertCreatedDTO;
import com.teknei.bid.dto.CertKey;
import com.teknei.bid.persistence.entities.BidClieCert;
import com.teknei.bid.persistence.entities.BidClieCertPK;
import com.teknei.bid.persistence.entities.BidConsSist;
import com.teknei.bid.persistence.repository.BidClieCertRepository;
import com.teknei.bid.persistence.repository.BidConsSistRepository;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.jolokia.util.Base64Util;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

@Component
public class CertGenerationService {

    @Value("${tkn.cert.absPath}")
    private String absPath;
    @Value("${tkn.cert.scriptStep01}")
    private String scriptStep01;
    @Value("${tkn.cert.wsdlStep01}")
    private String wsdlStep01;
    @Value("${tkn.cert.sslTarFile}")
    private String sslTarFileName;
    @Value("${tkn.cert.certPass}")
    private String certPass;
    @Autowired
    private BidClieCertRepository bidClieCertRepository;
    @Autowired
    private BidConsSistRepository bidConsSistRepository;
    @Value("${tkn.cert.secret.genCert}")
    private String secretName;
    private String pass;
    @Autowired
    private Decrypt decrypt;
    private static final String COD_CONS_SIST_NO_RETRY = "INTENTOS_CERT_PSC";
    private static final Integer NO_RETRY_FALLBACK = 3;
    private final static String[] CASES_NO_SERIAL_RETRIEVED = {"ALREADY_GENERATED_CERT", "BAD_CREDENTIALS", "NOT_SERIAL_READ"};
    private static final String REGEX_VALIDATE_CERT = "\\w+\\d+";
    @Value("${tkn.cert.isTest}")
    private boolean isTest;

    private static final Logger log = LoggerFactory.getLogger(CertGenerationService.class);

    @PostConstruct
    private void init(){
        initPassword();
    }

    private void initPassword(){
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".initPassword ");
        final String secretUri = "/run/secrets/"+secretName;
        pass = certPass;
        try {
            String content = new String(Files.readAllBytes(Paths.get(secretUri)));
            JSONObject jsonObject = new JSONObject(content);
            if (content == null || content.isEmpty()) {
                log.error("No secret supplied, leaving default");
            } else {
                pass = jsonObject.optString("password", certPass);
                pass = decrypt(pass);
            }
        } catch (IOException e) {
            log.error("No secret supplied, leaving default");
        }
    }

    public String findSerialByIdClient(Long idClie)
    {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".findSerial");
        List<BidClieCert> bidClieCerts = bidClieCertRepository.findAllByIdClieAndIdEsta(idClie, 1);
        if(bidClieCerts == null || bidClieCerts.isEmpty()){
            return null;
        }
        BidClieCert cert = bidClieCerts.get(0);
        String serial = cert.getSeriCert();
        //Validating requisites
        boolean matches = serial.matches(REGEX_VALIDATE_CERT);
        if(matches){
            return serial;
        }
        return null;
    }

    public String findSerial(Long idCustomer, String usernameRequesting) throws IllegalArgumentException 
    {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".findSerial ");
        BidClieCertPK pk = new BidClieCertPK();
        pk.setIdClie(idCustomer);
        pk.setNomUsua(usernameRequesting);
        BidClieCert retrieved = bidClieCertRepository.findOne(pk);
        if (retrieved == null) {
            throw new IllegalArgumentException("NO SERIAL FOUND IN DATABASE");
        } else {
            return retrieved.getSeriCert();
        }
    }

    private void saveCert(Long idCustomer, String serial, String usernameRequesting, String usernameCustomer, String obs) 
    {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".saveCert ");
        BidClieCert bidClieCert = new BidClieCert();
        bidClieCert.setFchCrea(new Timestamp(System.currentTimeMillis()));
        bidClieCert.setIdClie(idCustomer);
        bidClieCert.setIdEsta(1);
        bidClieCert.setIdTipo(3);
        bidClieCert.setInteGene(1);
        bidClieCert.setNomUsua(usernameCustomer);
        bidClieCert.setSeriCert(serial);
        bidClieCert.setUsrCrea(usernameRequesting);
        bidClieCert.setUsrOpeCrea(usernameRequesting);
        bidClieCert.setPassUsua("NA");
        bidClieCert.setObseCert(obs);
        bidClieCertRepository.save(bidClieCert);
    }

    public Integer getNoRetry() 
    {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getNoRetry ");
        List<BidConsSist> bidConsSistList = bidConsSistRepository.findAllByCodConsSistAndIdEsta(COD_CONS_SIST_NO_RETRY, 1);
        if (bidConsSistList == null || bidConsSistList.isEmpty()) {
            return NO_RETRY_FALLBACK;
        }
        BidConsSist active = bidConsSistList.get(0);
        try {
            return Integer.parseInt(active.getValConsSist());
        } catch (NumberFormatException e) {
            log.error("Invalid configuration in database for maximum retry: {}", e.getMessage());
            return NO_RETRY_FALLBACK;
        }
    }

    private void updateCert(BidClieCert cert, String obs, String usernameRequesting, String serial) 
    {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".updateCert ");
        log.info("Updating certificate with: |{}|", serial);
        cert.setUsrModi(usernameRequesting);
        cert.setFchModi(new Timestamp(System.currentTimeMillis()));
        cert.setUsrOpeModi(usernameRequesting);
        String previousObs = cert.getObseCert();
        if (previousObs == null) {
            previousObs = obs;
        } else {
            previousObs = new StringBuilder(previousObs).append("|").append(obs).toString();
        }
        cert.setObseCert(previousObs);
        Integer noRetry = cert.getInteGene();
        if (noRetry == null) {
            noRetry = 1;
        } else {
            noRetry = noRetry + 1;
        }
        cert.setInteGene(noRetry);
        if (!Arrays.asList(CASES_NO_SERIAL_RETRIEVED).contains(serial)) {
            log.info("Updating serial with: {}", serial);
            cert.setSeriCert(serial);
        } else {
            log.info("Updating not counting serial case: {}", serial);
        }
        bidClieCertRepository.save(cert);
    }

    public void saveOrUpdateCert(Long idCustomer, String serial, String usernameRequesting, String usernameCustomer, String obs) 
    {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".saveOrUpdateCert ");
        log.info("Save or update cert call");
        BidClieCertPK pk = new BidClieCertPK();
        pk.setIdClie(idCustomer);
        pk.setNomUsua(usernameCustomer);
        BidClieCert retrieved = bidClieCertRepository.findOne(pk);
        if (retrieved == null) {
            log.info("Save new cert");
            saveCert(idCustomer, serial, usernameRequesting, usernameCustomer, obs);
        } else {
            log.info("Update cert");
            updateCert(retrieved, obs, usernameRequesting, serial);
        }
    }


    public CertCreatedDTO generateCert(CertKey certKey) 
    {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".generateCert ");
        String sslTarFileUri = new StringBuilder(absPath).append("/").append(sslTarFileName).toString();
        String[] mailParts = certKey.getCustomerMail().split("@");
        String username = mailParts[0];
        String domain = mailParts[1];
        username = username.replace(" ", "_");
        domain = domain.replace(".", "_");
        String userCertG = username + "_" +certKey.getCustomerId();
        username = userCertG;
        String customerName = certKey.getCustomerName();
        customerName = customerName.replace(" ", "_");
        customerName = customerName.trim();
        StringBuilder builder = new StringBuilder();
        String isTestText = String.valueOf(isTest);
        builder
                .append("/usr/bin/python")
                .append(" ")
                .append(absPath).append("/").append(scriptStep01)
                .append(" ")
                .append(absPath)
                .append(" ")
                .append(customerName)
                .append(" ")
                .append(certKey.getCustomerId())
                .append(" ")
                .append(sslTarFileUri)
                .append(" ")
                .append(wsdlStep01)
                .append(" ")
                .append(certKey.getCustomerMail())
                .append(" ")
                .append(username)
                .append(" ")
                .append(certKey.getKey())
                .append(" ")
                .append(pass)
                .append(" ")
                .append(isTestText)
        ;
        log.debug("[tkn-service-contracts] ::::Invoking: {}", builder.toString());
        CommandLine cmdLine = CommandLine.parse(builder.toString());
        DefaultExecutor executor = new DefaultExecutor();
        try {
            String tempCert = certKey.getCustomerName();
            tempCert = tempCert.replace(" ","_");
            tempCert = tempCert.trim();

            executor.execute(cmdLine);
            String serialUri = new StringBuilder(absPath).append("/").append( tempCert ).append("-").append(certKey.getCustomerId()).append("/").append(certKey.getCustomerId()).append(username).append("_serial.txt").toString();

            String serialRetrieved = new String(Files.readAllBytes(Paths.get(serialUri)));
            return new CertCreatedDTO(serialRetrieved, 0, username);
        } catch (ExecuteException ee) {
            int exitCode = ee.getExitValue();
            log.error("Exit code reached: {}", exitCode);
            if (exitCode != 0) {
                switch (exitCode) {
                    case 403:
                        return new CertCreatedDTO("BAD_CREDENTIALS", 403, username);
                    case 409:
                        return new CertCreatedDTO("ALREADY_GENERATED_CERT", 409, username);
                    case 400:
                        return new CertCreatedDTO("NOT_SERIAL_READ", 422, username);
                    default:
                        String tempCert = certKey.getCustomerName();
                        tempCert = tempCert.replace(" ","_");
                        tempCert = tempCert.trim();

                        String errorCodeUri = new StringBuilder(absPath).append("/").append(tempCert).append("-").append(certKey.getCustomerId()).append("/").append(certKey.getCustomerId()).append(username).append("_error.code").toString();
                        try {
                            String errorCode = new String(Files.readAllBytes(Paths.get(errorCodeUri)));
                            Integer errCode = Integer.parseInt(errorCode);
                            switch (errCode) {
                                case 0:
                                    
                                    String serialUri = new StringBuilder(absPath).append("/").append(tempCert).append("-").append(certKey.getCustomerId()).append("/").append(certKey.getCustomerId()).append(username).append("_serial.txt").toString();

                                    String serialRetrieved = new String(Files.readAllBytes(Paths.get(serialUri)));
                                    return new CertCreatedDTO(serialRetrieved, 0, username);
                                case 403:
                                    return new CertCreatedDTO("BAD_CREDENTIALS", 403, username);
                                case 409:
                                    return new CertCreatedDTO("ALREADY_GENERATED_CERT", 409, username);
                                case 400:
                                    return new CertCreatedDTO("NOT_SERIAL_READ", 422, username);
                                default:
                                    log.error("No expected error code fallback: {}", errCode);
                                    return new CertCreatedDTO("PROVIDER_ERROR", 500, username);
                            }
                        } catch (Exception e) {
                            log.error("Could not retrieve fallback error code with message: {}", e.getMessage());
                            return new CertCreatedDTO("PROVIDER_ERROR", 500, username);
                        }
                }
            }
        } catch (IOException e) {
            log.error("Error invoking: {}", e.getMessage());
            return new CertCreatedDTO("PROVIDER_ERROR", 500, username);
        }
        return new CertCreatedDTO("PROVIDER_ERROR", 500, username);
    }

    private String decrypt(String source) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".decrypt");
        try {
            String decrypted = decrypt.decrypt(source);
            if(decrypted == null){
                return source;
            }
            return new String(Base64Util.decode(decrypted));
        } catch (Exception e) {
            log.warn("No ciphered content, returning clear");
            return source;
        }
    }
}
