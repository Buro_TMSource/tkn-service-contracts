package com.teknei.bid.service;

import com.teknei.bid.controller.rest.crypto.Decrypt;
import com.teknei.bid.persistence.entities.*;
import com.teknei.bid.persistence.repository.*;
import org.jolokia.util.Base64Util;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Service
public class ContractOTTService {

    private static final Logger log = LoggerFactory.getLogger(ContractOTTService.class);

    @Value("${tkn.contract.odt.odtTemplate}")
    private String odtTemplateUri;
    @Value("${tkn.contract.odt.odtTemplateSabadell}")
    private String odtSabadellTemplateUri;
    @Value("${tkn.contract.odt.pdfFolder}")
    private String pdfFolder;
    @Value("${tkn.contract.odt.pythonFileWithTs}")
    private String pythonFile;
    @Value("${tkn.contract.odt.pythonFileSabadellV1}")
    private String pythonFileSabadellv1;
    @Value("${tkn.contract.odt.pythonFileSabadellV2}")
    private String pythonFileSabadellv2;
    @Autowired
    private BidTipoFirmRepository tipoFirmRepository;
    @Autowired
    private BidClieFirmRepository bidClieFirmRepository;
    @Autowired
    private BidClieCertRepository bidClieCertRepository;
    private List<BidTipoFirm> tipoFirmList;
    private static final String ACCEPTANCE_NAME_APP_MOVIL = "APP_MOVI";
    private static BidTipoFirm bidTipoFirmAppMovi;
    private static final String ACCEPTANCE_NAME_CTA_RELA = "CTA_RELA";
    private static BidTipoFirm bidTipoFirmAppCtaRel;
    private static final String ACCEPTANCE_NAME_PAGA_SABA = "PAGA_SABA";
    private static BidTipoFirm bidTipoFirmAppPagaSaba;
    private static final String ACCEPTANCE_NAME_CREDIT_INFO = "AUTO_INFO_CRED";
    private static BidTipoFirm bidTipoFirmCreditInfo;
    private static final String ACCEPTANCE_NAME_SHARE_INFO = "AUTO_COMP_INFO";
    private static BidTipoFirm bidTipoFirmAppShareInfo;
    private static final String ACCEPTANCE_NAME_MERCHANDISING = "AUTO_FINE_MERC";
    private static BidTipoFirm bidTipoFirmAppMerchandising;
    @Value("${tkn.contract.odt.labels.customer.cert.serial}")
    private String certAuthenticityCustomerLabel;
    @Value("${tkn.contract.odt.labels.bank.cert.serial}")
    private String certAuthenticityBankLabel;
    @Value("${tkn.contract.odt.labels.customer.cert.date}")
    private String certAuthenticityCustomerDateLabel;
    @Value("${tkn.contract.odt.labels.bank.cert.date}")
    private String certAuthenticityBankDateLabel;
    @Value("${tkn.contract.odt.labels.sign.datePattern}")
    private String signDatePattern;
    @Autowired
    private BidClieNaadContRepository bidClieNaadContRepository;
    @Autowired
    private BidClieCiusContRepository bidClieCiusContRepository;
    @Autowired
    private BidClieBeneContRepository bidClieBeneContRepository;
    @Autowired
    private BidClieRfisContRepository bidClieRfisContRepository;
    @Autowired
    private BidClieRfisRepository bidClieRfisRepository;
    @Autowired
    private BidPaisRepository bidPaisRepository;
    @Autowired
    private BidClieCelRepository bidClieCelRepository;
    @Autowired
    private BidClieTelRepository bidClieTelRepository;
    private List<BidPais> listPais;
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private static final DateTimeFormatter formatterMidChar = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    private static final DateTimeFormatter formatterDayOnly = DateTimeFormatter.ofPattern("dd");
    private static final DateTimeFormatter formatterMonthOnly = DateTimeFormatter.ofPattern("MM");
    private static final DateTimeFormatter formatterMonthStringOnly = DateTimeFormatter.ofPattern("MMMM");
    private static final DateTimeFormatter formatterYearOnly = DateTimeFormatter.ofPattern("yyyy");
    private static final DateTimeFormatter formatterFull = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss");
    private DateTimeFormatter signDateFormatter;
    private String bankSerial;
    private String bankUsername;
    private String bankPassword;
    private String bankDate;
    @Value("${tkn.cert.secret.signDoc}")
    private String secretName02;
    @Autowired
    private Decrypt decrypt;


    @PostConstruct
    private void init() 
    {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".init ");
        initTipoFirmList();
        initPais();
        initPasswords02();
    }


    private void initPasswords02() {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".initPasswords02 ");
        final String secretUri = "/run/secrets/" + secretName02;
        try {
            String content = new String(Files.readAllBytes(Paths.get(secretUri)));
            JSONObject jsonObject = new JSONObject(content);
            if (content == null || content.isEmpty()) {
                log.error("No secret supplied, leaving default");
            } else {
                bankSerial = jsonObject.optString("bserial", "");
                bankUsername = jsonObject.optString("buser", "");
                bankPassword = jsonObject.optString("bpassword", "");
                bankDate = jsonObject.optString("bdate", "");
                bankSerial = decrypt(bankSerial);
                bankUsername = decrypt(bankUsername);
                bankPassword = decrypt(bankPassword);
                bankDate = decrypt(bankDate);
            }
        } catch (IOException e) {
            //bankUsername//TODO
            log.error("No secret supplied, leaving default");
        }
    }

    private void initTipoFirmList() {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".initTipoFirmList ");
        tipoFirmList = tipoFirmRepository.findAll();
        initTipoFirmObjects();
        signDateFormatter = DateTimeFormatter.ofPattern(signDatePattern);
    }

    private void initPais() {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+". ");
        listPais = bidPaisRepository.findAll();
    }

    private String findPaisName(String paisCode) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+". ");
        for (BidPais p : listPais) {
            if (p.getIdPais().equals(paisCode)) {
                return p.getNomPais();
            }
        }
        return "NA";
    }

    private void initTipoFirmObjects() {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".initTipoFirmObjects ");
        try {
            bidTipoFirmAppMovi = tipoFirmList.stream().filter(t -> t.getCodTipoFirm().equals(ACCEPTANCE_NAME_APP_MOVIL)).collect(Collectors.toList()).get(0);
            bidTipoFirmAppCtaRel = tipoFirmList.stream().filter(t -> t.getCodTipoFirm().equals(ACCEPTANCE_NAME_CTA_RELA)).collect(Collectors.toList()).get(0);
            bidTipoFirmAppPagaSaba = tipoFirmList.stream().filter(t -> t.getCodTipoFirm().equals(ACCEPTANCE_NAME_PAGA_SABA)).collect(Collectors.toList()).get(0);
            bidTipoFirmCreditInfo = tipoFirmList.stream().filter(t -> t.getCodTipoFirm().equals(ACCEPTANCE_NAME_CREDIT_INFO)).collect(Collectors.toList()).get(0);
            bidTipoFirmAppShareInfo = tipoFirmList.stream().filter(t -> t.getCodTipoFirm().equals(ACCEPTANCE_NAME_SHARE_INFO)).collect(Collectors.toList()).get(0);
            bidTipoFirmAppMerchandising = tipoFirmList.stream().filter(t -> t.getCodTipoFirm().equals(ACCEPTANCE_NAME_MERCHANDISING)).collect(Collectors.toList()).get(0);
        } catch (Exception e) {
            log.error("Error assigning valid value to BidClietipoFirm values with message: {}", e.getMessage());
            bidTipoFirmAppMovi = new BidTipoFirm();
            bidTipoFirmAppCtaRel = new BidTipoFirm();
            bidTipoFirmCreditInfo = new BidTipoFirm();
            bidTipoFirmAppShareInfo = new BidTipoFirm();
            bidTipoFirmAppMerchandising = new BidTipoFirm();
            bidTipoFirmAppPagaSaba = new BidTipoFirm();
            bidTipoFirmAppMovi.setIdTipoFirm(4l);
            bidTipoFirmAppCtaRel.setIdTipoFirm(5l);
            bidTipoFirmAppPagaSaba.setIdTipoFirm(6l);
            bidTipoFirmCreditInfo.setIdTipoFirm(7l);
            bidTipoFirmAppShareInfo.setIdTipoFirm(8l);
            bidTipoFirmAppMerchandising.setIdTipoFirm(9l);
        }
    }


    public byte[] generateContractSabadellWithArgumentsAndSerial(BidClieCont bidClieCont, long latitude, long longitude, String serialCustomerProvided, String serialDateCustomerProvided, String serialBankProvided, String serialBankDateProvided) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".generateContractSabadellWithArgumentsAndSerial 1 ");
    	//TODO get address in string from lat and long
        String signingAddress = "";
        LocalDateTime now = LocalDateTime.now();
        try {
            if (odtSabadellTemplateUri.endsWith("/")) {
                odtSabadellTemplateUri = odtSabadellTemplateUri.concat("/");
            }
            BidClieCont reference = modify(bidClieCont);
            //TODO change by previous todo
            signingAddress = reference.getDomi();
            String gender = "M";
            if (reference.getGeneClie().toUpperCase().equals("M")) {
                gender = "M";
            } else {
                gender = "F";
            }
            String isPEP = reference.getPepoExp();
            if (isPEP.toUpperCase().equals("S")) {
                isPEP = "Y";
            } else {
                isPEP = "N";
            }
            //log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".generateContractSabadellWithArgumentsAndSerial Procesando datos del cliente 1");
            String isParePEP = reference.getParePepoExp();
            if (isParePEP.toUpperCase().equals("S")) {
                isParePEP = "Y";
            } else {
                isParePEP = "N";
            }
            //log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".generateContractSabadellWithArgumentsAndSerial Procesando datos del cliente 2");
            String isAcceptedAppMovi = "Y";
            BidClieFirm acceptedMovi = bidClieFirmRepository.findByIdClieAndIdTipoFirm(bidClieCont.getIdClie(), bidTipoFirmAppMovi.getIdTipoFirm());
            if (acceptedMovi == null || acceptedMovi.getBolAcep() == null || acceptedMovi.getBolAcep() == false) {
                isAcceptedAppMovi = "N";
            }
            String isAcceptedCtaRel = "Y";
            BidClieFirm acceptedCtaRel = bidClieFirmRepository.findByIdClieAndIdTipoFirm(bidClieCont.getIdClie(), bidTipoFirmAppCtaRel.getIdTipoFirm());
            if (acceptedCtaRel == null || acceptedCtaRel.getBolAcep() == null || acceptedCtaRel.getBolAcep() == false) {
                isAcceptedCtaRel = "N";
            }
            String isAcceptedPagaSaba = "Y";
            BidClieFirm acceptedPagaSaba = bidClieFirmRepository.findByIdClieAndIdTipoFirm(bidClieCont.getIdClie(), bidTipoFirmAppPagaSaba.getIdTipoFirm());
            if (acceptedPagaSaba == null || acceptedPagaSaba.getBolAcep() == null || acceptedPagaSaba.getBolAcep() == false) {
                isAcceptedPagaSaba = "N";
            }
            String isAcceptedAuthorizationInformationCredit = "Y";
            BidClieFirm acceptedCredit = bidClieFirmRepository.findByIdClieAndIdTipoFirm(bidClieCont.getIdClie(), bidTipoFirmCreditInfo.getIdTipoFirm());
            if (acceptedCredit == null || acceptedCredit.getBolAcep() == null || acceptedCredit.getBolAcep() == false) {
                isAcceptedAuthorizationInformationCredit = "N";
            }
            String isAcceptedShareInfo = "Y";
            BidClieFirm acceptedShareInfo = bidClieFirmRepository.findByIdClieAndIdTipoFirm(bidClieCont.getIdClie(), bidTipoFirmAppShareInfo.getIdTipoFirm());
            if (acceptedShareInfo == null || acceptedShareInfo.getBolAcep() == null || acceptedShareInfo.getBolAcep() == false) {
                isAcceptedShareInfo = "N";
            }
            String isAcceptedMerchandising = "Y";
            BidClieFirm acceptedMerchandising = bidClieFirmRepository.findByIdClieAndIdTipoFirm(bidClieCont.getIdClie(), bidTipoFirmAppMerchandising.getIdTipoFirm());
            if (acceptedMerchandising == null || acceptedMerchandising.getBolAcep() == null || acceptedMerchandising.getBolAcep() == false) {
                isAcceptedMerchandising = "N";
            }
            String dateString = now.format(formatter);
            String serialCustomer = serialCustomerProvided;
            String serialCustomerDate = serialDateCustomerProvided;
            String serialBank = bankSerial;
            String serialBankDate = bankDate;
            Locale spanishLocale = new Locale("es", "ES");
            String dateSigning = now.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)
                    .withLocale(spanishLocale));
            dateSigning = dateSigning.replace(" ", "_");
            String dateDayOnly = now.format(formatterDayOnly);
            String monthOnly = now.format(formatterMonthOnly);
            String monthOnlyString = now.format(formatterMonthStringOnly.withLocale(spanishLocale));
            String yearOnly = now.format(formatterYearOnly);
            log.info("Year only: {}", yearOnly);
            String lastDigitYearMinusOne = yearOnly.substring(yearOnly.length() - 2, yearOnly.length() - 1);
            log.info("Last digit minus one: {}", lastDigitYearMinusOne);
            String lastDigitYear = yearOnly.substring(yearOnly.length() - 1);
            log.info("Last digit : {}", lastDigitYear);
            //log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".generateContractSabadellWithArgumentsAndSerial Procesando datos del cliente 4");
            String birthDateStr = reference.getFchNac();
            String birthDateStringParamToSend = "NA";
            LocalDate localDate = null;
            try {
                localDate = LocalDate.parse(birthDateStr, formatterMidChar);
            } catch (Exception e) {
                log.error("Error trying to parse date: {1:lblancas} with format: {}", birthDateStr, formatterMidChar);
                try {
                    localDate = LocalDate.parse(birthDateStr, formatter);
                } catch (Exception e2) {
                    log.error("Error trying to parse date: {2:lblancas} with format: {}", birthDateStr, formatter);
                    log.error("Unable to parse with known formats, assigning NA");
                }
            }
            if (localDate != null) {
                birthDateStringParamToSend = localDate.format(formatter);
            }
            //log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".generateContractSabadellWithArgumentsAndSerial Procesando datos del cliente 5");
            String nationalities = "NA";
            List<BidClieNaadCont> naads = bidClieNaadContRepository.findAllByIdClie(bidClieCont.getIdClie());
            if (naads != null && !naads.isEmpty()) {
                StringBuilder naadsSB = new StringBuilder();
                naads.forEach(n -> naadsSB.append(n.getNaciAdi()).append(",_"));
                naadsSB.deleteCharAt(naadsSB.length() - 1);
                naadsSB.deleteCharAt(naadsSB.length() - 1);
                nationalities = naadsSB.toString();
            }
            String isAmerican = "N";
            if (reference.getCiudEua().toUpperCase().equals("S")) {
                isAmerican = "Y";
            }
            String citicenships = "NA";
            List<BidClieCiusCont> citicens = bidClieCiusContRepository.findAllByIdClie(bidClieCont.getIdClie());
            if (citicens != null && !citicens.isEmpty()) {
                StringBuilder citySB = new StringBuilder();
                citicens.forEach(n -> citySB.append(n.getCiudAdi()).append(",_"));
                citySB.deleteCharAt(citySB.length() - 1);
                citySB.deleteCharAt(citySB.length() - 1);
                citicenships = citySB.toString();
            }
            //log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".generateContractSabadellWithArgumentsAndSerial Procesando datos del cliente 6");
            List<BidClieBeneCont> beneContList = bidClieBeneContRepository.findAllByIdClie(bidClieCont.getIdClie());
            List<BidClieBeneCont> beneContListParam = new ArrayList<>();
            if (beneContList != null && !beneContList.isEmpty()) {
                if (beneContList.size() > 5) {
                    beneContListParam = beneContList.subList(0, 4);
                } else {
                    beneContListParam = beneContList;
                    int size = beneContListParam.size();
                    int remain = 5 - size;
                    for (int i = 0; i < remain; i++) {
                        BidClieBeneCont cont = new BidClieBeneCont();
                        cont.setDescPare("NA");
                        cont.setDomiBene("NA");
                        cont.setFchNac("NA");
                        cont.setNombBene("NA");
                        cont.setPorcPart(null);
                        beneContListParam.add(cont);
                    }
                }
            } else {
                for (int i = 0; i < 5; i++) {
                    BidClieBeneCont cont = new BidClieBeneCont();
                    cont.setDescPare("NA");
                    cont.setDomiBene("NA");
                    cont.setFchNac("NA");
                    cont.setNombBene("NA");
                    cont.setPorcPart(null);
                    beneContListParam.add(cont);
                }
            }
            //log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".generateContractSabadellWithArgumentsAndSerial Procesando datos del cliente 7");
            StringBuilder bidClieBeneString = new StringBuilder();
            beneContListParam.forEach(b ->
                    bidClieBeneString.append(b.getNombBene() == null || b.getNombBene().isEmpty() ? "NA" : b.getNombBene().trim().replace(" ", "_"))
                            .append(" ").append(b.getDescPare() == null || b.getDescPare().isEmpty() ? "NA" : b.getDescPare().replace(" ", "_"))
                            .append(" ").append(b.getFchNac() == null || b.getFchNac().isEmpty() ? "NA" : b.getFchNac().replace(" ", "_"))
                            .append(" ").append(b.getPorcPart() == null ? "NA" : b.getPorcPart())
                            .append(" ").append(b.getDomiBene() == null || b.getDomiBene().isEmpty() ? "NA" : b.getDomiBene().replace(" ", "_")).append(" "));
            List<BidClieRfisCont> rfisContList = bidClieRfisContRepository.findAllByIdClie(bidClieCont.getIdClie()); 
            //log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".generateContractSabadellWithArgumentsAndSerial Procesando datos del cliente 8");
            List<BidClieRfisCont> rfisContListParam = new ArrayList<>();
            if (rfisContList != null && !rfisContList.isEmpty()) {
                if (rfisContList.size() > 4) {
                    rfisContListParam = rfisContList.subList(0, 4);
                } else {
                    rfisContListParam = rfisContList;
                    int remain = 4 - rfisContListParam.size();
                    for (int i = 0; i < remain; i++) {
                        BidClieRfisCont cont = new BidClieRfisCont();
                        cont.setNomPais("NA");
                        cont.setRazNoTin("NA");
                        cont.setTin("NA");
                        rfisContListParam.add(cont);
                    }
                }
            } else {
                for (int i = 0; i < 4; i++) {
                    BidClieRfisCont cont = new BidClieRfisCont();
                    cont.setNomPais("NA");
                    cont.setRazNoTin("NA");
                    cont.setTin("NA");
                    rfisContListParam.add(cont);
                }
            }
            BidClieCel bidClieCel=null;
            BidClieTel bidClieTel=null;
            try
            {
            	bidClieCel = bidClieCelRepository.findByIdClie(bidClieCont.getIdClie());
            	log.info("Cel obtenido por repositori ",bidClieCel.getCel() + " --- " + bidClieCont.getIdClie());
            }catch(Exception e )
            {
            	log.info("+++++++ No trae datos en Select * from  bid.clie_cel where id_clie ="+bidClieCont.getIdClie());
            	bidClieCel=new BidClieCel();
            }
            try
            {
            	bidClieTel = bidClieTelRepository.findByIdClie(bidClieCont.getIdClie());
            	log.info("Tel obtenido por repositori ",bidClieTel.getTel() + " --- " + bidClieCont.getIdClie());

            }catch(Exception e )
            {
            	log.info("+++++++ No trae datos en Select * from  bid.clie_tel where id_clie ="+bidClieCont.getIdClie());
            	 bidClieTel=new BidClieTel();
            }
            //log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".generateContractSabadellWithArgumentsAndSerial Termina las validaciones del cliente");
            StringBuilder rfisBuilder = new StringBuilder();
            rfisContListParam.forEach(r -> rfisBuilder.append(
                    r.getNomPais() == null || r.getNomPais().isEmpty() ? "NA" : r.getNomPais().replace(" ", "_")).append(" ")
                    .append(r.getTin() == null || r.getTin().isEmpty() ? "NA" : r.getTin().replace(" ", "_")).append(" ")
                    .append(r.getRazNoTin() == null || r.getRazNoTin().isEmpty() ? "NA" : r.getRazNoTin().replace(" ", "_")).append(" "));
            String commandBuilder = new StringBuilder("python")
                    .append(" ").append(pythonFileSabadellv2).append(" ")
                    .append(odtSabadellTemplateUri).append(" ")
                    .append(pdfFolder).append("/ ")
                    .append(bidClieCont.getIdClie()).append(" ")
                    .append(pdfFolder).append("/").append(bidClieCont.getIdClie()).append(".odt ")
                    .append(reference.getNomClie()).append(" ")
                    .append(reference.getApePate()).append(" ")
                    .append(reference.getApeMate()).append(" ")
                    .append(gender).append(" ")
                    .append(reference.getFchNac()).append(" ")
                    .append(reference.getLugNac()).append(" ")
                    .append(reference.getPaisNac()).append(" ")
                    .append(reference.getNaci()).append(" ")
                    .append(reference.getCurp()).append(" ")
                    .append(reference.getRfc()).append(" ")
                    .append(reference.getDocuIden()).append(" ")
                    .append(reference.getNumeDocuIden()).append(" ")
                    .append("NA").append(" ") //TODO fiel missing
                    .append(reference.getDomi()).append(" ")
                    .append(reference.getCol()).append(" ")
                    .append(reference.getMuni()).append(" ")
                    .append(reference.getEsta()).append(" ")
                    .append(reference.getCp()).append(" ")
                    .append(reference.getPais()).append(" ")
                    .append(bidClieTel == null || bidClieTel.getTel() == null || bidClieTel.getTel().isEmpty() ? "NA" : bidClieTel.getTel()).append(" ")
                    .append(bidClieCel == null || bidClieCel.getCel() == null || bidClieCel.getCel().isEmpty() ? "NA" : bidClieCel.getCel()).append(" ")
                    .append(reference.getEmai()).append(" ")
                    .append(reference.getActi()).append(" ")
                    .append(reference.getIngMensBrt()).append(" ")
                    .append(isPEP).append(" ")
                    .append(isParePEP).append(" ")
                    .append(reference.getNombPepoExp()).append(" ")
                    .append(reference.getPare()).append(" ")
                    .append("Y").append(" ") //TODO missing actua por cuenta propia
                    .append("Y").append(" ") //TODO missing deposito a tercero
                    .append(isAcceptedAppMovi).append(" ")
                    .append(isAcceptedCtaRel).append(" ")
                    .append(isAcceptedPagaSaba).append(" ")
                    .append(dateString).append(" ")
                    .append(serialCustomer).append(" ")
                    .append(serialCustomerDate).append(" ")
                    .append(serialBank).append(" ")
                    .append(serialBankDate).append(" ")
                    .append(isAcceptedAuthorizationInformationCredit).append(" ")
                    .append(isAcceptedShareInfo).append(" ")
                    .append(isAcceptedMerchandising).append(" ")
                    .append(signingAddress).append(" ")
                    .append(dateSigning).append(" ")
                    .append(reference.getCtaCorr()).append(" ")
                    .append(reference.getCtaClab()).append(" ")
                    .append(dateDayOnly).append(" ")
                    .append(monthOnly).append(" ")
                    .append(yearOnly).append(" ")
                    .append(reference.getBanco()).append(" ")
                    .append(reference.getCtaClabDest()).append(" ")
                    .append(reference.getAliaCta()).append(" ")
                    .append(reference.getMontMax() == null ? "0.0" : reference.getMontMax()).append(" ")
                    .append(monthOnlyString).append(" ")
                    .append(lastDigitYearMinusOne).append(" ")
                    .append(lastDigitYear).append(" ")
                    .append(reference.getDomi()).append(" ")
                    .append("NA").append(" ")
                    .append("NA").append(" ")
                    .append("NA").append(" ")
                    .append(birthDateStringParamToSend).append(" ")
                    .append(nationalities).append(" ")
                    .append(isAmerican).append(" ")
                    .append(citicenships).append(" ")
                    .append(bidClieBeneString.toString())
                    .append(rfisBuilder.toString())
                    .append(reference.getTasIntCta() == null || reference.getTasIntCta().isEmpty() ? "NA" : reference.getTasIntCta()).append(" ")
                    .append(reference.getGatNomCta() == null || reference.getGatNomCta().isEmpty() ? "NA" : reference.getGatNomCta()).append(" ")
                    .append(reference.getGatRealCta() == null || reference.getGatRealCta().isEmpty() ? "NA" : reference.getGatRealCta()).append(" ")
                    .append(reference.getTasIntPag() == null || reference.getTasIntPag().isEmpty() ? "NA" : reference.getTasIntPag()).append(" ")
                    .append(reference.getGatNomPag() == null || reference.getGatNomPag().isEmpty() ? "NA" : reference.getGatNomPag()).append(" ")
                    .append(reference.getGatRealPag() == null || reference.getGatRealPag().isEmpty() ? "NA" : reference.getGatRealPag()).append(" ")
                    .append(reference.getHolder() == null  ? "NA" : reference.getHolder()).append(" ")
                    .toString();
            commandBuilder = commandBuilder.replace("(", "_");
            commandBuilder = commandBuilder.replace(")", "_");
            //log.info("[tkn-service-contracts] ::::  Invoking: {}", commandBuilder);
            Process p = Runtime.getRuntime().exec(commandBuilder);
            p.waitFor();
            String pdfUri = new StringBuilder(pdfFolder).append("/").append(bidClieCont.getIdClie()).append(".pdf").toString();
            byte[] contract = Files.readAllBytes(Paths.get(pdfUri));
            Files.delete(Paths.get(pdfUri));
            //log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".generateContractSabadellWithArgumentsAndSerial Termina con exito la generacion del contrato");
            return contract;
        } catch (Exception e) {
            log.error("Error generating contract for sabadell v2 with message: {}", e.getMessage());
            log.error("Error generating contract for sabadell v2 with error: {}", e);
            return null;
        }
    }

    public byte[] generateContractWithTs(String name, String lastname, String curp, String documentNumber, String address, String date, String mail, String cel, String tel, Long operationId, String certSerial, String dateTimestamp) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".generateContractWithTs 2");
    	try {
            if (odtTemplateUri.endsWith("/")) {
                odtTemplateUri = odtTemplateUri.concat("/");
            }
            name = name.replace(" ", "_");
            date = date.replace(" ", "_");
            if (cel == null || cel.isEmpty()) {
                cel = tel;
                if (cel == null || cel.isEmpty()) {
                    cel = "NA";
                }
            }
            cel = cel.replace(" ", "");
            address = address.replace(" ", "_");
            lastname = lastname.replace(" ", "_");
            String fullName = new StringBuilder(name).append("_").append(lastname).toString();
            String commandBuilder = new StringBuilder("python")
                    .append(" ").append(pythonFile).append(" ")
                    .append(odtTemplateUri).append(" ")
                    .append(pdfFolder).append("/ ")
                    .append(operationId).append(" ")
                    .append(pdfFolder).append("/").append(operationId).append(".odt ")
                    .append(fullName).append(" ")
                    .append(curp).append(" ")
                    .append(date).append(" ")
                    .append(cel).append(" ")
                    .append(mail).append(" ")
                    .append(address).append(" ")
                    .append(certSerial).append(" ")
                    .append(dateTimestamp).append(" ")
                    .toString();
            //log.info("lblancas::::: Invoking: {}", commandBuilder);
            Process p = Runtime.getRuntime().exec(commandBuilder);
            p.waitFor();
            String pdfUri = new StringBuilder(pdfFolder).append("/").append(operationId).append(".pdf").toString();
            byte[] contract = Files.readAllBytes(Paths.get(pdfUri));
            Files.delete(Paths.get(pdfUri));
            return contract;
        } catch (Exception e) {
            log.error("Error generating contract with message: {}", e.getMessage());
            return null;
        }

    }

    public byte[] generateContract(String name, String lastname, String curp, String documentNumber, String address, String date, String mail, String cel, String tel, Long operationId) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".generateContract 3 ");
    	try {
            if (odtTemplateUri.endsWith("/")) {
                odtTemplateUri = odtTemplateUri.concat("/");
            }
            name = name.replace(" ", "_");
            date = date.replace(" ", "_");
            if (cel == null || cel.isEmpty()) {
                cel = tel;
                if (cel == null || cel.isEmpty()) {
                    cel = "NA";
                }
            }
            cel = cel.replace(" ", "");
            address = address.replace(" ", "_");
            lastname = lastname.replace(" ", "_");
            String fullName = new StringBuilder(name).append("_").append(lastname).toString();
            String commandBuilder = new StringBuilder("python")
                    .append(" ").append(pythonFile).append(" ")
                    .append(odtTemplateUri).append(" ")
                    .append(pdfFolder).append("/ ")
                    .append(operationId).append(" ")
                    .append(pdfFolder).append("/").append(operationId).append(".odt ")
                    .append(fullName).append(" ")
                    .append(curp).append(" ")
                    .append(date).append(" ")
                    .append(cel).append(" ")
                    .append(mail).append(" ")
                    .append(address).append(" ")
                    .toString();
            //log.info("[tkn-service-contracts] :: "+this.getClass().getName()+". commandBuilder  >> "+commandBuilder);
            Process p = Runtime.getRuntime().exec(commandBuilder);
            p.waitFor();
            String pdfUri = new StringBuilder(pdfFolder).append("/").append(operationId).append(".pdf").toString();
            byte[] contract = Files.readAllBytes(Paths.get(pdfUri));
            Files.delete(Paths.get(pdfUri));
            return contract;
        } catch (Exception e) {
            log.error("Error generating contract with message: {}", e.getMessage());
            return null;
        }
    }

    private BidClieCont modify(BidClieCont source) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".modify ");
        Class<?> clazz = source.getClass();
        Class<?> clazzString = String.class;
        try {
            Field[] fields = clazz.getDeclaredFields();
            for (Field f : fields) {
                Class<?> clazzType = f.getType();
                if (clazzType.isAssignableFrom(clazzString)) {
                    f.setAccessible(true);
                    String previousValue = (String) f.get(source);
                    if (previousValue == null || previousValue.isEmpty()) {
                        previousValue = "NA";
                    }
                    previousValue = previousValue.toString().replace(" ", "_");
                    previousValue = previousValue.toString().replace("(", "_");
                    previousValue = previousValue.toString().replace(")", "_");
                    f.set(source, previousValue);
                }
            }
        } catch (Exception e) {
            log.error("Error assigning reflection value: {}", e.getMessage());
        }
        return source;
    }

    private String decrypt(String source) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".decrypt ");
        try {
            String decrypted = decrypt.decrypt(source);
            if (decrypted == null) {
                return source;
            }
            return new String(Base64Util.decode(decrypted));
        } catch (Exception e) {
            log.warn("No ciphered content, returning clear");
            return source;
        }
    }


}
