package com.teknei.bid.service;

import com.teknei.bid.controller.rest.crypto.Decrypt;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.jolokia.util.Base64Util;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class ContractNom151Service {

    @Value("${tkn.contract.signed.javaFullPath}")
    private String fullJavaLocation;
    @Value("${tkn.cert.absPath}")
    private String tempAbs;
    @Value("${tkn.contract.signed.tempPath}")
    private String tempFolder;
    @Value("${tkn.cert.scriptStep03}")
    private String jarNameNom151;
    @Value("${tkn.cert.wsdlStep03}")
    private String wsdlStep03;
    @Value("${tkn.cert.step03User}")
    private String nom151User;
    @Value("${tkn.cert.step03Password}")
    private String nom151Password;
    private static final String suffix = "_const_hash.pdf";
    @Value("${tkn.cert.secret.nom151}")
    private String secretName03;
    @Autowired
    private Decrypt decrypt;

    private static final Logger log = LoggerFactory.getLogger(ContractNom151Service.class);

    @PostConstruct
    private void init() {
        initPasswords03();
    }

    private void initPasswords03() {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".initPasswords03 ");
        final String secretUri = "/run/secrets/" + secretName03;
        String user = nom151User;
        String password = nom151Password;
        try {
            String content = new String(Files.readAllBytes(Paths.get(secretUri)));
            JSONObject jsonObject = new JSONObject(content);
            if (content == null || content.isEmpty()) {
                log.error("No secret supplied, leaving default");
            } else {
                user = jsonObject.optString("user", nom151User);
                password = jsonObject.optString("password", nom151Password);
                user = decrypt(user);
                password = decrypt(password);
            }
        } catch (IOException e) {
            log.error("No secret supplied, leaving default");
        }
        nom151User = user;
        nom151Password = password;
    }

    public byte[] signContract(Long idCustomer, byte[] contract) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".signContract 11 ");
        byte[] nom151 = null;
        String username = "Usernom151_" + idCustomer;
        String finalUriSignedTemp = new StringBuilder(tempFolder).append("/").append(username).append(".pdf").toString();
        try {
            Files.write(Paths.get(finalUriSignedTemp), contract);
            log.info("Written file: {}", finalUriSignedTemp);
        } catch (IOException e) {
            log.error("Error to temporary write signed with fingers document for customer: {} with error: {}", idCustomer, e.getMessage());
            log.error("Trace: ", e);
            return null;
        }
        StringBuilder sb = new StringBuilder(fullJavaLocation).append(" -jar ")
                .append(tempAbs).append("/").append(jarNameNom151).append(" ")
                .append(wsdlStep03).append(" ")
                .append(nom151User).append(" ")
                .append(nom151Password).append(" ")
                .append(finalUriSignedTemp);
        //log.info("[tkn-service-contracts]  Invoking: {}", sb.toString());
        String fu = finalUriSignedTemp.substring(0, finalUriSignedTemp.length() - 4);
        String finalUri = new StringBuilder(fu).append(suffix).toString();
        try {
            CommandLine cmdLine = CommandLine.parse(sb.toString());
            log.info("Debug de comand line: {}", cmdLine.toString());
            DefaultExecutor executor = new DefaultExecutor();
            int exitValue = executor.execute(cmdLine);
            log.info("Exit value: {}", exitValue);
            nom151 = Files.readAllBytes(Paths.get(finalUri));
        } catch (Exception e) {
            log.error("Error signing with customer data with message: {}", e.getMessage());
        }
        try {
            Files.delete(Paths.get(finalUriSignedTemp));
        } catch (Exception e) {
            log.error("Unable to delete signed pdf with error: {}", e.getMessage());
        }
        return nom151;
    }

    private String decrypt(String source) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".decrypt ");
        try {
            String decrypted = decrypt.decrypt(source);
            if (decrypted == null) {
                return source;
            }
            return new String(Base64Util.decode(decrypted));
        } catch (Exception e) {
            log.warn("No ciphered content, returning clear");
            return source;
        }
    }
}
