package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ContractSignedDTO implements Serializable {

    private String fileB64;
    private Long operationId;
    private String username;

}