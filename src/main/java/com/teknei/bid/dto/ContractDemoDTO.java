package com.teknei.bid.dto;


import lombok.Data;

import java.io.Serializable;

@Data
public class ContractDemoDTO implements Serializable {

	private String nombre;
	private String curp;
	private String domicilio;
	private String huella;
	private boolean conhuella;
	@Override
	public String toString() {
		return "SignContractRequestDTO [nombre=" + nombre  
				+ ", curp=" + curp + ", domicilio=" + domicilio + ", conhuella=" + conhuella + "]";
	}
    
} 