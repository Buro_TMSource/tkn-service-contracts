package com.teknei.bid.command;

import lombok.Data;

import java.io.Serializable;
import java.util.Arrays;

@Data
public class CommandResponse implements Serializable{

    private Status status;
    private String desc;
    private long id;
    private String scanId;
    private String documentId;
    private String curp;
    byte[] contract;
    byte[] signedContract;
	public String imprime() {
		return "CommandResponse [status=" + status + ", desc=" + desc + ", id=" + id + ", scanId=" + scanId
				+ ", documentId=" + documentId + ", curp=" + curp + ", contract=" + (Arrays.toString(contract) == null ?  "lblancas:NULO" :  "lblancas:FULL")
				+ ", signedContract=" + (Arrays.toString(signedContract) == null ?  "lblancas:NULO" :  "lblancas:FULL")  + "]";
	}

}