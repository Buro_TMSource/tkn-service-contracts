package com.teknei.bid.command.impl.contract;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.persistence.entities.*;
import com.teknei.bid.persistence.repository.*;
import com.teknei.bid.service.ContractExstreamService;
import com.teknei.bid.service.ContractOTTService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class ParseContractCommand implements Command {

    private static final Logger log = LoggerFactory.getLogger(ParseContractCommand.class);

    @Autowired
    private ContractExstreamService contractExstreamService;
    @Autowired
    private BidDireNestRepository bidDireNestRepository;
    @Autowired
    private BidClieRepository bidClieRepository;
    @Autowired
    private BidCurpRepository bidCurpRepository;
    @Autowired
    private BidDireRepository bidDireRepository;
    @Autowired
    private BidMailRepository bidMailRepository;
    @Autowired
    private BidTelRepository bidTelRepository;
    @Autowired
    private BidIfeRepository bidIfeRepository;
    @Autowired
    private ContractOTTService contractOTTService;
    @Autowired
    private BidCliePasaRepository bidCliePasaRepository;
    @Value("${tkn.contract.provider}")
    private String contractProvider;
    private static final SimpleDateFormat sdf = new SimpleDateFormat("MMM d, yyyy");

    @Override
    public CommandResponse execute(CommandRequest request) {
    	//log.info("[tkn-service-contracts]:: "+this.getClass().getName()+".execute ");
        CommandResponse response = new CommandResponse();
        response.setId(request.getId());
        String address = "";
        try {
            BidClieDirePK direPK = new BidClieDirePK();
            direPK.setIdDire(request.getId());
            direPK.setIdClie(request.getId());
            BidClieDire clieDire = bidDireRepository.findOne(direPK);
            if (clieDire == null) {
                BidClieDireNestPK bidClieDireNestPK = new BidClieDireNestPK();
                bidClieDireNestPK.setIdClie(request.getId());
                bidClieDireNestPK.setIdDire(request.getId());
                BidClieDireNest bidClieDireNest = bidDireNestRepository.findOne(bidClieDireNestPK);
                if (bidClieDireNest != null) {
                    address = bidClieDireNest.getDirNestDos();
                	
                } else {
                	address = new StringBuilder(" ").toString();	
                }
            } else {
                address = new StringBuilder(clieDire.getCall()).append(" ").append(clieDire.getCol()).append(" ").append(clieDire.getCp()).toString();
            }

            BidClieIfeInePK ifeInePK = new BidClieIfeInePK();
            ifeInePK.setIdIfe(request.getId());
            ifeInePK.setIdClie(request.getId());
            BidClieIfeIne ifeIne = null;
            BidCliePasa bidCliePasa = null;
            String ocr = "NA";
            try {
                ifeIne = bidIfeRepository.findOne(ifeInePK);
                if (ifeIne != null) {
                    ocr = ifeIne.getOcr();
                } else {
                    BidCliePasaPK pasaPK = new BidCliePasaPK();
                    pasaPK.setIdPasa(request.getId());
                    pasaPK.setIdClie(request.getId());
                    BidCliePasa pasa = bidCliePasaRepository.findOne(pasaPK);
                    if (pasa != null) {
                        ocr = pasa.getNumPasa();
                    }
                }
            } catch (Exception e) {
                log.error("Error finding OCR value: {}", e.getMessage());
            }
            BidClie bidClie = bidClieRepository.findOne(request.getId());
            BidClieCurp bidClieCurp = bidCurpRepository.findTopByIdClie(request.getId());
            String name = bidClie.getNomClie();
            String lastName = new StringBuilder(bidClie.getApePate()).append(" ").append(bidClie.getApeMate()).toString();
            String curp = bidClieCurp.getCurp();
            String dateStr = sdf.format(new Date());
            BidClieMail bidClieMail = bidMailRepository.findTopByIdClie(request.getId());
            BidTel bidTel = bidTelRepository.findTopByIdClie(request.getId());
            String mail = "example@mail.com";
            String tel = "1122334455";
            if (bidClieMail != null) {
                mail = bidClieMail.getEmai();
            }
            if (bidTel != null) {
                tel = bidTel.getTele();
            }
            byte[] contract = null;
            if (contractProvider.toUpperCase().contains("ODT")) {
                contract = contractOTTService.generateContract(name, lastName, curp, ocr, address, dateStr, mail, tel, tel, request.getId());
            } else {
                contract = contractExstreamService.generateContract(name, lastName, curp, ocr, address, dateStr, mail, tel, tel);
            }
            response.setContract(contract);
            response.setStatus(Status.CONTRACT_EXSTREAM_OK);
        } catch (Exception e) {
            log.error("Erorr in parseContractCommand for request: {} with message: {}", request, e.getMessage());
            e.printStackTrace();
            response.setStatus(Status.CONTRACT_EXSTREAM_ERROR);
        }
        return response;
    }


}
