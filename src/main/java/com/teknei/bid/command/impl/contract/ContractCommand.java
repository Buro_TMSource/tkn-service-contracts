package com.teknei.bid.command.impl.contract;

import com.teknei.bid.command.*;
import com.teknei.bid.persistence.entities.BidClieRegEsta;
import com.teknei.bid.persistence.entities.BidEstaProc;
import com.teknei.bid.persistence.repository.BidClieRegEstaRepository;
import com.teknei.bid.persistence.repository.BidEstaProcRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class ContractCommand implements Command {

    @Autowired
    @Qualifier(value = "parseContractCommand")
    private Command parseContractCommand;
    @Autowired
    @Qualifier(value = "persistContractCommand")
    private Command persistContractCommand;
    @Autowired
    @Qualifier(value = "storeTasContractCommand")
    private Command storeTasContractCommand;
    @Autowired
    @Qualifier(value = "statusCommand")
    private Command statusCommand;
    @Autowired
    @Qualifier(value = "parseContractSignedCommand")
    private Command parseContractSignedCommand;
    @Autowired
    private BidClieRegEstaRepository regEstaRepository;
    @Autowired
    private BidEstaProcRepository bidEstaProcRepository;
    @Autowired
    private BidClieRegEstaRepository bidClieRegEstaRepository;
    @Autowired
    @Qualifier(value = "parseContractCommandWithSerialTS")
    private Command parseContractCommandWithSerialTs;
    @Autowired
    @Qualifier(value = "parseContractCommandWithSabadellV1")
    private Command parseContractCommandWithSabadellV1;
    private static final String ESTA_PROC = "CONS-CONT";
    private static final String ESTA_PROC_2 = "AUT-DAC";
    private static final String ESTA_PROC_3="FIR-CONT";
    @Value("${tkn.contract.type.withSerial}")
    private Boolean withSerial;
    @Value("${tkn.contract.type.withSabadellV1}")
    private Boolean withSabadellV1;
    private static final Logger log = LoggerFactory.getLogger(ContractCommand.class);

    @Override
    public CommandResponse execute(CommandRequest request) {
    	log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".execute ");
    	log.info("withSabadellV1:::"+withSabadellV1);
    	log.info("withSerial:::::::"+withSerial);
    	log.info("withSabadellV1:::"+withSabadellV1);
    	log.info("withSabadellV1::[1]:  "+request.getRequestType());
    	log.info("withSabadellV1::[2]:  "+RequestType.CONTRACT_REQUEST_UNSIGNED);
    	log.info("withSabadellV1::[3]:  "+RequestType.CONTRACT_REQUEST_SIGNED);
    	
        byte[] contract = null;
        byte[] signedContract = null;
        if (request.getRequestType().equals(RequestType.CONTRACT_REQUEST_UNSIGNED)) {
            CommandResponse parseResponse = null;
            if(withSabadellV1){
                parseResponse = parseContractCommandWithSabadellV1.execute(request);
            }else if(withSerial){
                parseResponse = parseContractCommandWithSerialTs.execute(request);
            }else{
                parseResponse = parseContractCommand.execute(request);
            }
            log.info("parseResponse : {}",parseResponse);
            if (parseResponse.getStatus().equals(Status.CONTRACT_EXSTREAM_ERROR)) {
                parseResponse.setDesc(String.valueOf(parseResponse.getStatus().getValue()));
                parseResponse.setStatus(Status.CONTRACT_ERROR);
                saveStatus(request.getId(), Status.CONTRACT_EXSTREAM_ERROR, request.getUsername());
                return parseResponse;
            }
            contract = parseResponse.getContract();
        }
        else if (request.getRequestType().equals(RequestType.CONTRACT_REQUEST_SIGNED)) 
        {
        	log.info("LBL:::parseContractSignedCommand.execute");
            CommandResponse parseSignedResponse = parseContractSignedCommand.execute(request);
            if (parseSignedResponse.getStatus().equals(Status.CONTRACT_ERROR)) {
                parseSignedResponse.setDesc(String.valueOf(parseSignedResponse.getStatus().getValue()));
                parseSignedResponse.setStatus(Status.CONTRACT_ERROR);
                saveStatus(request.getId(), Status.CONTRACT_EXSTREAM_ERROR, request.getUsername());
                return parseSignedResponse;
            }
            signedContract = parseSignedResponse.getSignedContract();
        } else {
            signedContract = request.getSignedContract();
        }
        log.info("LBL:::Salva Estaus");
        saveStatus(request.getId(), Status.CONTRACT_EXSTREAM_OK, request.getUsername());
        CommandRequest tasRequest = new CommandRequest();
        tasRequest.setId(request.getId());
        if (request.getRequestType().equals(RequestType.CONTRACT_REQUEST_UNSIGNED)) {
            tasRequest.setRequestType(RequestType.CONTRACT_REQUEST_UNSIGNED);
            tasRequest.setContract(contract);
        } else {
            tasRequest.setSignedContract(signedContract);
            tasRequest.setRequestType(RequestType.CONTRACT_REQUEST_SIGNED);
        }
        CommandResponse tasResponse = storeTasContractCommand.execute(tasRequest);
        log.info("LBL:::Salva TAS");
        if (tasResponse.getStatus().equals(Status.CONTRACT_TAS_UNSIGNED_ERROR) || tasResponse.getStatus().equals(Status.CONTRACT_TAS_SIGNED_ERROR)) {
            saveStatus(request.getId(), tasResponse.getStatus(), request.getUsername());
            tasResponse.setDesc(String.valueOf(tasResponse.getStatus().getValue()));
            tasResponse.setStatus(Status.CONTRACT_ERROR);
            return tasResponse;
        }
        if (request.getRequestType().equals(RequestType.CONTRACT_REQUEST_UNSIGNED)) {
            saveStatus(request.getId(), Status.CONTRACT_TAS_UNSIGNED_OK, request.getUsername());
        } else {
            saveStatus(request.getId(), Status.CONTRACT_TAS_SIGNED_OK, request.getUsername());
        }
        log.info("LBL:::Modifica BD de contracts");
        CommandRequest dbRequest = new CommandRequest();
        dbRequest.setUsername(request.getUsername());
        dbRequest.setId(request.getId());
        CommandResponse dbResponse = persistContractCommand.execute(dbRequest);
        //TODO verify if this saveStatus has to be part for unsigned and part for signed contract as with tas and parse process
        if (dbResponse.getStatus().equals(Status.CONTRACT_DB_ERROR)) {
            dbResponse.setDesc(String.valueOf(dbResponse.getStatus().getValue()));
            dbResponse.setStatus(Status.CONTRACT_ERROR);
            saveStatus(request.getId(), Status.CONTRACT_DB_ERROR, request.getUsername());
            return dbResponse;
        }
        saveStatus(request.getId(), Status.CONTRACT_DB_OK, request.getUsername());
        log.info("LBL:::Modifica BD de contracts::"+Status.CONTRACT_DB_OK);
        dbResponse.setStatus(Status.CONTRACT_OK);
        if (request.getRequestType().equals(RequestType.CONTRACT_REQUEST_UNSIGNED)) {
            updateStatus(request.getId(), ESTA_PROC, request.getUsername());
        } else {
            updateStatus(request.getId(), ESTA_PROC_2, request.getUsername());
            updateStatus(request.getId(), ESTA_PROC_3, request.getUsername());
        }
        dbResponse.setContract(contract);
        dbResponse.setSignedContract(signedContract);
        log.info("LBL:::Termina con estatus ::"+request.getRequestType().equals(RequestType.CONTRACT_REQUEST_UNSIGNED));
        return dbResponse;
    }

    /**
     * Persists the current status for the main process
     *
     * @param id
     * @param status
     * @return
     */
    private CommandResponse saveStatus(Long id, Status status, String username) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".saveStatus ");
        CommandRequest request = new CommandRequest();
        request.setId(id);
        request.setRequestStatus(status);
        request.setUsername(username);
        CommandResponse response = statusCommand.execute(request);
        return response;
    }

    private void updateStatus(Long idClient, String status, String username) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".updateStatus ");
        try {
            BidEstaProc estaProc = bidEstaProcRepository.findTopByCodEstaProcAndIdEsta(status, 1);
            BidClieRegEsta regEsta = bidClieRegEstaRepository.findByIdClieAndIdEstaProc(idClient, estaProc.getIdEstaProc());
            if (regEsta == null) {
                log.warn("Status for process: {} found null", idClient);
            }
            regEsta.setEstaConf(true);
            regEsta.setFchModi(new Timestamp(System.currentTimeMillis()));
            regEsta.setUsrModi(username);
            regEsta.setUsrOpeModi(username);
            bidClieRegEstaRepository.save(regEsta);
        } catch (Exception e) {
            log.error("Error finding status of process for customer: {} with message: {}", idClient, e.getMessage());
        }
    }
}
