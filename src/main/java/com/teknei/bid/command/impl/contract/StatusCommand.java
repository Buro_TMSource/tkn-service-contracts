package com.teknei.bid.command.impl.contract;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.persistence.entities.BidClieCurp;
import com.teknei.bid.persistence.entities.BidClieRegProcStatus;
import com.teknei.bid.persistence.repository.BidCurpRepository;
import com.teknei.bid.persistence.repository.BidRegProcStatusRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class StatusCommand implements Command {

    @Autowired
    private BidRegProcStatusRepository bidRegProcStatusRepository;
    @Autowired
    private BidCurpRepository bidCurpRepository;

    private static final Logger log = LoggerFactory.getLogger(StatusCommand.class);

    @Override
    public CommandResponse execute(CommandRequest request) 
    {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".execute  Se guarda en Base de datos lo siguiente:");
        CommandResponse response = new CommandResponse();
        response.setId(request.getId());
        Long id = request.getId();
        BidClieCurp bidClieCurp = bidCurpRepository.findTopByIdClie(id);

        BidClieRegProcStatus bidClieRegProcStatus = new BidClieRegProcStatus();
        bidClieRegProcStatus.setCurp(bidClieCurp.getCurp());
        bidClieRegProcStatus.setFchCrea(new Timestamp(System.currentTimeMillis()));
        bidClieRegProcStatus.setIdClie(id);
        bidClieRegProcStatus.setIdEsta(1);
        bidClieRegProcStatus.setIdTipo(3);
        bidClieRegProcStatus.setUsrCrea(request.getUsername());
        bidClieRegProcStatus.setUsrOpeCrea(request.getUsername());
        bidClieRegProcStatus.setIdStatus((long) request.getRequestStatus().getValue());
        try {
            bidRegProcStatusRepository.save(bidClieRegProcStatus);
            response.setStatus(Status.ADDRESS_DB_STATUS_OK);
        } catch (Exception e) {
            log.error("Error in save regProcStatus with message: {}", e.getMessage());
            response.setStatus(Status.ADDRESS_DB_STATUS_ERROR);
        }
        log.info("Curp        ::"+bidClieRegProcStatus.getCurp());
        log.info("FchCrea     ::"+bidClieRegProcStatus.getFchCrea());
        log.info("IdClie      ::"+bidClieRegProcStatus.getIdClie());
        log.info("IdEsta      ::"+bidClieRegProcStatus.getIdEsta());
        log.info("IdTipo      ::"+bidClieRegProcStatus.getIdTipo());
        log.info("UsrCrea     ::"+bidClieRegProcStatus.getUsrCrea());
        log.info("UsrOpeCrea  ::"+bidClieRegProcStatus.getUsrOpeCrea());
        log.info("IdStatus    ::"+bidClieRegProcStatus.getIdStatus()); 
        
        return response;
    }
}
