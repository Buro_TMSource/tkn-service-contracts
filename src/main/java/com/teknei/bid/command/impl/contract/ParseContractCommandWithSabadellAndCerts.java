package com.teknei.bid.command.impl.contract;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.persistence.entities.BidClieCert;
import com.teknei.bid.persistence.entities.BidClieCont;
import com.teknei.bid.persistence.repository.BidClieCertRepository;
import com.teknei.bid.persistence.repository.BidClieContRepository;
import com.teknei.bid.service.ContractOTTService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Component
public class ParseContractCommandWithSabadellAndCerts implements Command {

    private static final Logger log = LoggerFactory.getLogger(ParseContractCommandWithSabadellAndCerts.class);

    @Autowired
    private ContractOTTService contractOTTService;
    @Autowired
    private BidClieCertRepository bidClieCertRepository;
    @Autowired
    private BidClieContRepository bidClieContRepository;
    @Value("${tkn.contract.provider}")
    private String contractProvider;
    private static final SimpleDateFormat sdf = new SimpleDateFormat("MMM d, yyyy");
    private Integer noRetry = 3; //TODO get this for config

    @Override
    public CommandResponse execute(CommandRequest request) {
    	//log.info("[tkn-service-contracts]:: "+this.getClass().getName()+".execute ");
        CommandResponse response = new CommandResponse();
        response.setId(request.getId());
        try {
            byte[] contract = null;
            String serialCustomer = "NA";
            String serialCustomerDate = "NA";
            String serialBank = "NA";
            String serialBankDate = "NA";
/*
            List<BidClieCert> certList = bidClieCertRepository.findAllByIdClieAndIdEsta(request.getId(), 1);
            if (certList != null && !certList.isEmpty()) {
                BidClieCert cert = certList.get(0);
                serialCustomer = cert.getSeriCert();
                serialCustomerDate = sdf.format(cert.getFchCrea());
                serialCustomerDate = serialCustomerDate.replace(" ", "_");
            }
*/
            log.info("ParseContractCommandWithSabadellAndCerts :execute -> Se modifica Serie y Fecha");
            
            BidClieCont clieCont = bidClieContRepository.findByIdClie(request.getId());
            clieCont = modify(clieCont);
            
            Date curDate = new Date(); 
            serialCustomerDate= sdf.format(curDate);
            serialCustomerDate = serialCustomerDate.replace(" ", "_"); 
            serialCustomer=sha1(""+clieCont.getIdClie());
            
            log.info("Operation Id   :"+request.getId() + "  serialCustomer>> "+serialCustomer+ "    serialCustomerDate>>>>"+serialCustomerDate);
            
            
            contract = contractOTTService.generateContractSabadellWithArgumentsAndSerial(clieCont, 0l, 0l, serialCustomer, serialCustomerDate, serialBank, serialBankDate);
            if (contract == null) {
                for (int i = 0; i < noRetry; i++) {
                    contract = contractOTTService.generateContractSabadellWithArgumentsAndSerial(clieCont, 0l, 0l, serialCustomer, serialCustomerDate, serialBank, serialBankDate);
                    if (contract != null) {
                        break;
                    }
                }
            }
            log.info("(contract == null)     :"+(contract == null));
            if (contract == null) {
                response.setStatus(Status.CONTRACT_PREFILLED_ERROR);
            } else {
                response.setStatus(Status.CONTRACT_PREFILLED_OK);
            }
            response.setContract(contract);
        } catch (Exception e) {
            log.error("Erorr in parseContractCommand With serial TS for request: {} with message: {}", request, e.getMessage());
            e.printStackTrace();
            response.setStatus(Status.CONTRACT_PREFILLED_ERROR);
        }
        return response;
    }
    public   String sha1(String input) throws NoSuchAlgorithmException {
        MessageDigest mDigest = MessageDigest.getInstance("SHA1");
        byte[] result = mDigest.digest(input.getBytes());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }
         
        return sb.toString();
    }
    private BidClieCont modify(BidClieCont source) {
    	//log.info("[tkn-service-contracts]:: "+this.getClass().getName()+".modify ");
        Class<?> clazz = source.getClass();
        Class<?> clazzString = String.class;
        try {
            Field[] fields = clazz.getDeclaredFields();
            for (Field f : fields) {
                Class<?> clazzType = f.getType();
                if (clazzType.isAssignableFrom(clazzString)) {
                    f.setAccessible(true);
                    String previousValue = (String) f.get(source);
                    if (previousValue == null) {
                        previousValue = "NA";
                    }
                    previousValue = previousValue.toString().replace(" ", "_");
                    f.set(source, previousValue);
                }
            }
        } catch (Exception e) {
            log.error("Error assigning reflection value: {}", e.getMessage());
        }
        return source;
    }


}
