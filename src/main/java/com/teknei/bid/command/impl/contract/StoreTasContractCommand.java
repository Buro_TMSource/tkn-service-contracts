package com.teknei.bid.command.impl.contract;

import com.teknei.bid.command.*;
import com.teknei.bid.service.ContractTasService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StoreTasContractCommand implements Command {

    @Autowired
    private ContractTasService contractTasService;
    private static final Logger log = LoggerFactory.getLogger(StoreTasContractCommand.class);

    @Override
    public CommandResponse execute(CommandRequest request) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".execute ");
        CommandResponse commandResponse = new CommandResponse();
        commandResponse.setId(request.getId());
        try {
            if (request.getRequestType().equals(RequestType.CONTRACT_REQUEST_UNSIGNED)) {
                contractTasService.addContractToTas(request.getContract(), request.getId());
                commandResponse.setStatus(Status.CONTRACT_TAS_UNSIGNED_OK);
            } else if(request.getRequestType().equals(RequestType.CONTRACT_REQUEST_SIGNED)) {
                contractTasService.addContactSignedToTas(request.getSignedContract(), request.getId());
                commandResponse.setStatus(Status.CONTRACT_TAS_SIGNED_OK);
            } else if(request.getRequestType().equals(RequestType.CONTRACT_REQUEST_PREFILLED)){
                contractTasService.addContactPrefilledWithCertToTas(request.getContract(), request.getId());
                commandResponse.setStatus(Status.CONTRACT_TAS_SIGNED_OK);
            }else if(request.getRequestType().equals(RequestType.CONTRACT_REQUEST_NOM151_EVIDENCE)){
                contractTasService.addContactNom151EvidencetToTas(request.getContract(), request.getId());
                commandResponse.setStatus(Status.CONTRACT_TAS_SIGNED_OK);
            }else{
                throw new IllegalArgumentException("No request type known: " + request.getRequestType());
            }
        } catch (Exception e) {
            log.error("Error in storeTasContractCommand for: {} with message: {}", request, e.getMessage());
            if (request.getRequestType().equals(RequestType.CONTRACT_REQUEST_UNSIGNED)) {
                commandResponse.setStatus(Status.CONTRACT_TAS_UNSIGNED_ERROR);
            } else {
                commandResponse.setStatus(Status.CONTRACT_TAS_SIGNED_ERROR);
            }
        }
        return commandResponse;
    }


}
