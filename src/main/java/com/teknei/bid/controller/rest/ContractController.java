package com.teknei.bid.controller.rest;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.teknei.bid.command.*;
import com.teknei.bid.dto.ContractDemoDTO;
import com.teknei.bid.dto.ContractSignedDTO;
import com.teknei.bid.dto.SignContractRequestDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Date;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/contract")
public class ContractController {

    @Autowired
    @Qualifier("contractCommand")
    private Command contractCommand;
    @Autowired
    @Qualifier("contractCommandWithCerts")
    private Command contractCommandWithCerts;

    private static final Logger log = LoggerFactory.getLogger(ContractController.class);
    
    @RequestMapping(value = "/contrato/sign", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> signContract(@RequestBody SignContractRequestDTO contractRequestDTO) {
        log.debug("Requesting /contrato/sign: {}", contractRequestDTO);
        try {
            CommandRequest commandRequest = new CommandRequest();
            commandRequest.setRequestType(RequestType.CONTRACT_REQUEST_SIGNED);
            commandRequest.setData(contractRequestDTO.getBase64Finger());
            commandRequest.setData2(contractRequestDTO.getContentType());
            commandRequest.setId(contractRequestDTO.getOperationId());
            commandRequest.setUsername(contractRequestDTO.getUsername());
            commandRequest.setScanId(contractRequestDTO.getHash());
            CommandResponse response = contractCommand.execute(commandRequest);
            if (response.getStatus().equals(Status.CONTRACT_OK)) {
                return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            return new ResponseEntity<>("50005", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Generates contract for the current customer given by id", response = byte[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the contract is generated successfully"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/contrato/{id}/{username}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getUnsignedContract(@PathVariable("id") Long id, @PathVariable("username") String username) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContract ");
        byte[] contract = new byte[0];
        try {
            CommandRequest request = new CommandRequest();
            request.setRequestType(RequestType.CONTRACT_REQUEST_UNSIGNED);
            request.setId(id);
            request.setUsername(username);
            CommandResponse response = contractCommand.execute(request);
            if (response.getStatus().equals(Status.CONTRACT_OK)) {
                contract = response.getContract();
                return new ResponseEntity<>(contract, HttpStatus.OK);
            } else {
                return new ResponseEntity<>((byte[]) null, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error generating contract for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>(contract, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Generates contract pre filled for the current customer given by id", response = byte[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the contract is generated successfully"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/contractWithCert/{id}/{username}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getUnsignedContractWithCerts(@PathVariable("id") Long id, @PathVariable("username") String username){
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContractWithCerts ");
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContractWithCerts id      :"+id);
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContractWithCerts username:"+username);
        byte[] contract = new byte[0];
        try{
            CommandRequest request = new CommandRequest();
            request.setRequestType(RequestType.CONTRACT_REQUEST_PREFILLED);
            request.setId(id);
            request.setUsername(username);
            CommandResponse response = contractCommandWithCerts.execute(request);
            //log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContractWithCerts response.getStatus():"+response.getStatus());
            if (response.getStatus().equals(Status.CONTRACT_OK)) 
            {
            	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContractWithCerts response.getStatus(): ENTRO A TRUE");
                contract = response.getContract();
                return new ResponseEntity<>(contract, HttpStatus.OK);
            }
            else 
            {
            	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContractWithCerts response.getStatus(): ENTRO A FALSE");
                return new ResponseEntity<>((byte[]) null, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        }catch (Exception e) {
            log.error("Error generating contract for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>(contract, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Generates contract for the current customer given by id", response = byte[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the contract is generated successfully"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/contratoB64/{id}/{username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getUnsignedContractB64(@PathVariable("id") Long id, @PathVariable("username") String username) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContractB64 ");
        byte[] contract = new byte[0];
        String contractB64 = "";
        JSONObject jsonObject = new JSONObject();
        try {
            CommandRequest request = new CommandRequest();
            request.setRequestType(RequestType.CONTRACT_REQUEST_UNSIGNED);
            request.setId(id);
            request.setUsername(username);
            CommandResponse response = contractCommand.execute(request);
            if (response.getStatus().equals(Status.CONTRACT_OK)) {
                contract = response.getContract();
                contractB64 = Base64Utils.encodeToString(contract);
                jsonObject.put("contract", contractB64);
                return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
            } else {
                jsonObject.put("contract", "");
                return new ResponseEntity<>(jsonObject.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error generating contract for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Uploads contract signed and stores in the document manager")
    @RequestMapping(value = "/uploadPlain", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> addPlainSignedContract(@RequestBody ContractSignedDTO contractSignedDTO) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".addPlainSignedContract ");
        try {
            CommandRequest request = new CommandRequest();
            request.setId(contractSignedDTO.getOperationId());
            request.setUsername(contractSignedDTO.getUsername());
            request.setContract(Base64Utils.decodeFromString(contractSignedDTO.getFileB64()));
            CommandResponse response = contractCommand.execute(request);
            if (response.getStatus().equals(Status.CONTRACT_OK)) {
                return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error adding signed contract with message: {}", e.getMessage());
            return new ResponseEntity<>("50005", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Deprecated
    @ApiOperation(value = "Adds signed contract to the document casefile", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the contract is generated successfully"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/upload/{id}", method = RequestMethod.POST)
    public ResponseEntity<String> addContratoFirmado(@RequestPart(value = "file") MultipartFile file, @PathVariable("id") Long id) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".addContratoFirmado ");
        try {
            CommandRequest request = new CommandRequest();
            request.setId(id);
            request.setRequestType(RequestType.CONTRACT_REQUEST_SIGNED_MOBILES);
            request.setContract(file.getBytes());
            request.setUsername("NA");
            CommandResponse response = contractCommand.execute(request);
            if (response.getStatus().equals(Status.CONTRACT_OK)) {
                return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error adding signed contract for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>("50005", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    
    @ApiOperation(value = "Uploads contract signed and stores in the document manager")
    @RequestMapping(value = "/demo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getDemo(@RequestBody ContractDemoDTO dto) 
    {
    	//log.info("lblancas: "+this.getClass().getName()+".{getContractDemo() }"+dto.toString());
    	log.info("Valores "+  dto.toString());
    	byte[] responseEntity =null;
    	try
    	{
    		log.info("Intenta con  : "+dto.isConhuella());
    		responseEntity =getContractDemoLocaly(dto.getNombre(),dto.getCurp(),dto.getDomicilio(),dto.isConhuella());
    	}catch(Exception e)
    	{
    		log.info("Intenta sin imagen");
    		responseEntity =getContractDemoLocaly(dto.getNombre(),dto.getCurp(),dto.getDomicilio(),false);
    	}
 
    	return new ResponseEntity<>(Base64Utils.encodeToString( responseEntity) , HttpStatus.OK); 
    }
    
    /** 
     * Esto es un ejemplo
     * @param nombre
     * @param curp
     * @param domicilio
     * @param huella
     * @return
     */ 
    private byte[] getContractDemoLocaly(String NOMBRE, String CURP, String DOMICILIO,boolean conImagen)
	{
		 ByteArrayOutputStream stream=null;
 /*
		 byte[] huellaUTF8 =null;
		 if(conImagen) 
			 huellaUTF8 = encodeBase64(getImagen("/home/huellaC.jpg"));
	*/ 
		 try 
		 {
			//log.info("Crea documento en memoria");
		   Document document = new Document(PageSize.A4, 35, 30, 50, 50);
				 
  		   stream = new ByteArrayOutputStream();
  		   //String pdfNewFile="/home/demo_"+(new Date()).getTime()+".pdf";
  		   //PdfWriter.getInstance(document, new FileOutputStream(pdfNewFile));
		   PdfWriter.getInstance(document, stream);
		    
		   // Abrir el documento
		   document.open();
		    
		   Image image = null;
		 
		   //log.info("Crear las fuentes para el contenido y los titulos");
		   // Crear las fuentes para el contenido y los titulos
		   Font fontContenido = FontFactory.getFont(
		     FontFactory.TIMES_ROMAN.toString(), 11, Font.NORMAL,
		     BaseColor.DARK_GRAY);
		   
		   Font fontSubrayado = FontFactory.getFont(
				     FontFactory.TIMES_ROMAN.toString(), 11, Font.UNDERLINE,
				     BaseColor.DARK_GRAY);	
		   
		   Font fontTitulos = FontFactory.getFont(
		     FontFactory.TIMES_BOLDITALIC, 11, Font.UNDERLINE,
		     BaseColor.BLACK);
		   // Creacion del parrafo
		   Paragraph paragraph0 = new Paragraph();
		 
		   // Agregar un titulo con su respectiva fuente
		   paragraph0.add(new Phrase("CONTRATO DE AMISTAD", fontTitulos));
		 
		   //log.info("Agregar saltos de linea");
		   // Agregar saltos de linea
		   paragraph0.add(new Phrase(Chunk.NEWLINE));
		   paragraph0.add(new Phrase(Chunk.NEWLINE));
		   paragraph0.setAlignment(Element.ALIGN_CENTER);
		   document.add(paragraph0);
		   Date f=new Date();
		   int dia=f.getDate();
		   int mes=f.getMonth()+1;
		   int year=f.getYear()+1900;
		   Paragraph paragraph = new Paragraph();
		   paragraph.add(new Phrase("Ciudad de México de "+dia +" de "+ MONTH(mes)+ " de " +year , fontTitulos));
			
		   // Agregar saltos de linea
		   paragraph.add(new Phrase(Chunk.NEWLINE));
		   paragraph.add(new Phrase(Chunk.NEWLINE));
		    
		   // Agregar contenido con su respectiva fuente
		   paragraph.add(new Phrase("CONTRATO ENTRE “ ",fontContenido));
		 
		   paragraph.add(new Phrase(NOMBRE,fontSubrayado));
		   
		   paragraph.add(new Phrase(" ” DE AHORA EN ADELANTE “EL MEJOR AMIGO” Y BURÓ DE IDENTIDAD DIGITAL DE AHORA EN ADELANTE “BID” "
		   		+ "PARA CELEBRAR EL INICIO DE UNA AMISTAD LARGA Y DURADERA SIN FINES DE LUCRO.",
			       fontContenido));
		   
		   //log.info(" DE AHORA EN ADELANTE,....");
		  
		   paragraph.add(new Phrase(Chunk.NEWLINE)); 
		   paragraph.add(new Phrase(Chunk.NEWLINE)); 
		   paragraph.add(new Phrase("EL MEJOR AMIGO CON CURP: ", fontContenido));
		   paragraph.add(new Phrase( CURP , fontSubrayado));
		   paragraph.add(new Phrase("  Y CON DOMICILIO CONOCIDO EN:  " , fontContenido));
		   paragraph.add(new Phrase(DOMICILIO , fontSubrayado));
		   paragraph.add(new Phrase( " ,  ACEPTA QUE LOS DATOS ANTES MENCIONADOS SON CORRECTOS Y QUE HAN SIDO EXTRAÍDOS DE MANERA AUTOMÁTICA "
		   		+ "MEDIANTE UN PROCESO DE EXTRACCIÓN DE DATOS “OCR” AGILIZANDO EL PROCESO PRESENTADO POR BID DURANTE LA DEMOSTRACIÓN.", fontContenido));
		   
		   paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
		   document.add(paragraph);
		   paragraph=new Paragraph();
		   
		   paragraph.add(new Phrase(Chunk.NEWLINE));  
		   
		   //log.info(" SERVICIOS INTEGRALES OFRECIDOS POR BID,....");
		   paragraph.add(new Phrase("SERVICIOS INTEGRALES OFRECIDOS POR BID", fontTitulos));
 
		   paragraph.setAlignment(Element.ALIGN_LEFT);
		   document.add(paragraph);
		   paragraph=new Paragraph();
		   
		   paragraph.add(new Phrase("INTEGRAMOS LAS MEJORES TECNOLOGÍAS DISPONIBLES OFRECIENDO UN PROCESO END TO END (E2E) PARA CREAR UNA VERDADERA"
		   		+ " EXPERIENCIA DIGITAL PARA SU CLIENTE Y FACILITAR UN ENROLAMIENTO RÁPIDO Y SEGURO, DANDO ADEMÁS LA SEGURIDAD Y CERTEZA DE QUE ES "
		   		+ "QUIEN DICE SER GRACIAS A LA IMPLEMENTACIÓN DE TECNOLOGÍAS DE VALIDACIÓN BIOMÉTRICA Y TESTIFICACIÓN DE DOCUMENTOS ANTE DIVERSAS ENTIDADES.", fontContenido));
		   paragraph.add(new Phrase(Chunk.NEWLINE)); 
		   paragraph.add(new Phrase(Chunk.NEWLINE)); 
		   paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
		   document.add(paragraph);
		   
		   paragraph=new Paragraph();
		   
		   //log.info(" ENROLAMIENTO BIOMÉTRICO DIGITAL,....");
		   paragraph.add(new Phrase("ENROLAMIENTO BIOMÉTRICO DIGITAL", fontTitulos));
		   paragraph.setAlignment(Element.ALIGN_LEFT);
		   document.add(paragraph);
		   paragraph=new Paragraph();  
		   paragraph.add(new Phrase("A CUALQUIER HORA Y DESDE CUALQUIER LUGAR, ALGUIEN PUEDE SER SU NUEVO CLIENTE.", fontContenido));
		   paragraph.add(new Phrase(Chunk.NEWLINE)); 
		   paragraph.add(new Phrase(Chunk.NEWLINE));	
		   paragraph.add(new Phrase("CREAMOS UNA VERDADERA EXPERIENCIA DIGITAL PARA SUS CLIENTES, REDISEÑANDO EL PROCESO DE ENROLAMIENTO DE NUEVOS CLIENTES, IMPLEMENTADO UN PROCESO E2E, SIN PAPEL, RÁPIDO Y SUMAMENTE SEGURO.", fontContenido));
		   paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
		   document.add(paragraph);
		   paragraph=new Paragraph();
		   paragraph.add(new Phrase(Chunk.NEWLINE));
		   
		   paragraph.add(new Phrase("REGISTRO Y VALIDACIÓN BIOMÉTRICA", fontTitulos));
		   paragraph.setAlignment(Element.ALIGN_LEFT);
		   document.add(paragraph);
		   paragraph=new Paragraph();  
		   paragraph.add(new Phrase("CONOZCO A MI CLIENTE, ES QUIEN DICE SER", fontContenido));
		   paragraph.add(new Phrase(Chunk.NEWLINE)); 
		   paragraph.add(new Phrase("EL MECANISMO MÁS EFICIENTE PARA IDENTIFICAR A SU CLIENTE, PROVEEDOR O EMPLEADO ES LA IMPLEMENTACIÓN DE REGISTRO Y VALIDACIÓN "
		   		+ " BIOMÉTRICA DENTRO DE LOS PROCESOS DE NEGOCIO QUE REQUIERAN LA IDENTIFICACIÓN DE QUIENES INTERACTÚAN EN ESA TRANSACCIÓN.", fontContenido));
		   paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
		   document.add(paragraph);
		   paragraph=new Paragraph();
		   paragraph.add(new Phrase(Chunk.NEWLINE));
		   paragraph.add(new Phrase(Chunk.NEWLINE)); 
		   document.add(paragraph);
		    
		   //log.info(" Tabla PDF....");  
		   PdfPTable table = new PdfPTable(5);
		   float dimension[]={80,80,40,30,80};
		   table.setTotalWidth(dimension);
		   // Agregar la imagen anterior a una celda de la tabla
		   PdfPCell cel10 = new PdfPCell(new Phrase(" "));
		   cel10.setFixedHeight(50);
		   cel10.setBorder(Rectangle.NO_BORDER);
		   cel10.setColspan(1);
	       table.addCell(cel10);
	       
	       
	       Paragraph FIRMA = new Paragraph();
	       FIRMA.add(new Phrase(NOMBRE, fontTitulos));
		   FIRMA.add(new Phrase(Chunk.NEWLINE));  	
		   FIRMA.add(new Phrase(Chunk.NEWLINE));
		   FIRMA.add(new Phrase("EL MEJOR AMIGO", fontContenido));
		   FIRMA.setAlignment(Element.ALIGN_CENTER);
		   PdfPCell cel20 = new PdfPCell(FIRMA);
		   cel20.setColspan(2);
		   cel20.setBorder(Rectangle.NO_BORDER);
		   cel20.setFixedHeight(50);
	       table.addCell(cel20);
	       
	       if(conImagen)
	       {
	    	   try
	    	   {
			       PdfPCell cel30 =  null; 
			       log.info("Estamos tomando la imagen : /home/huellaC.jpg" );
		    	   image = Image.getInstance("/home/huellaC.jpg");
		    	   image.scaleAbsolute(50f, 50f);
		    	   cel30= new PdfPCell(image); 
			       cel30.setBorder(Rectangle.NO_BORDER);
			       cel30.setColspan(1);
			       cel30.setFixedHeight(50);
			       table.addCell(cel30);  
	    	   }
	    	   catch(Exception e)
	    	   {
	    		   PdfPCell cel30 =  null; 
			       log.info("Estamos tomando la imagen : \\home\\huellaC.jpg" );
		    	   image = Image.getInstance("\\home\\huellaC.jpg");
		    	   image.scaleAbsolute(50f, 50f);
		    	   cel30= new PdfPCell(image); 
			       cel30.setBorder(Rectangle.NO_BORDER);
			       cel30.setColspan(1);
			       cel30.setFixedHeight(50);
			       table.addCell(cel30);
	    	   }
	       }
	       else
	       {
	    	   PdfPCell cel30 = new PdfPCell(new Phrase(" "));
		       cel30.setBorder(Rectangle.NO_BORDER);
		       cel30.setColspan(1);
		       cel30.setFixedHeight(50);
		       table.addCell(cel30); 
	       }
	       PdfPCell cel40 = new PdfPCell(new Phrase(" "));
	       cel40.setFixedHeight(50);
	       cel40.setBorder(Rectangle.NO_BORDER);
	       cel40.setColspan(1);
	       table.addCell(cel40);  
	       //log.info(" Crea tabla "); 
	       
		   document.add(table); 
		   // Cerrar el documento
		   document.close();
		   //log.info(" Cierro doc "); 
		   // Abrir el archivo
		   //File file = new File(fileName);
		   //Desktop.getDesktop().open();
		  } catch (Exception ex) {
		   ex.printStackTrace();
		  }
		 log.info(" fin "); 
		 return stream.toByteArray();
	}
    /**
     * Obtiene mes depende del numero de mes
     * @param mes
     * @return
     */
    private static String MONTH(int mes) 
	{
		if(mes==1)return "Enero";
		if(mes==2)return "Ferbero";
		if(mes==3)return "Marzo";
		if(mes==4)return "Abril";
		if(mes==5)return "Mayo";
		if(mes==6)return "Junio";
		if(mes==7)return "Julio";
		if(mes==8)return "Agosto";
		if(mes==9)return "Septiembre";
		if(mes==10)return "Octubre";
		if(mes==11)return "Noviembre";
		if(mes==112)return "Diciembre";
		return "Enero";
	}
    /**
     * Codifica a Base 64
     * @param encodedBytes
     * @return
     */
    private byte[] encodeBase64(byte[]  encodedBytes)
	{
		return  Base64.getEncoder().encode(encodedBytes);
	}
	/**
	 * Service para decodificar Base 64.
	 * @param encodedBytes
	 * @return
	 */
	private byte[] decodeBase64(byte[]  arrayByte )
	{
		return  Base64.getDecoder().decode(arrayByte);
	}
	private byte[] getImagen(String fileHuella)
	{	
		byte contenido[]=null;
		try {
			File fichero = new java.io.File(fileHuella); 
			FileInputStream ficheroStream = new FileInputStream(fichero); 
			contenido = new byte[(int)fichero.length()]; 
			ficheroStream.read(contenido);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return contenido;
	}
}
