package com.teknei.bid.controller.rest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.teknei.bid.service.ContractPSCSignService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/test")
public class TestSignController {
	private static final Logger log = LoggerFactory.getLogger(TestSignController.class);
    @Autowired
    private ContractPSCSignService pscSignService;

    @RequestMapping(value = "sign", method = RequestMethod.POST)
    public byte[] signDocument(@RequestBody String json){
    	//log.info("lblancas:  [tkn-service-contracts] :: "+this.getClass().getName()+". ");
        JSONObject jsonObject = new JSONObject(json);
        String docb64 = jsonObject.getString("doc");
        String certUser1 = jsonObject.getString("certUser1");
        String certPass1 = jsonObject.getString("certPass1");
        String certSerial1 = jsonObject.getString("certSerial1");
        String certUser2 = jsonObject.getString("certUser2");
        String certPass2 = jsonObject.getString("certPass2");
        String certSerial2 = jsonObject.getString("certSerial2");
        byte[] content = Base64Utils.decodeFromString(docb64);
        byte[] signed = null;
        for (int i = 0; i<5; i++) {
            signed = pscSignService.testSignDocument(content, certUser1, certPass1, certSerial1, "temp1", 1l);
            if(signed != null){
                break;
            }
        }
        byte[] signed2 = null;
        for (int i = 0; i<5; i++) {
            signed2 = pscSignService.testSignDocument(signed, certUser2, certPass2, certSerial2, "temp1", 1l);
            if(signed2 != null){
                break;
            }
        }
        return signed2;

    }

}
