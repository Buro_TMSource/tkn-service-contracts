package com.teknei.bid.controller.rest.crypto;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

@Service
public class Decrypt {

    @Value("${tkn.crypto.url}")
    private String keyURL;
    @Value("${tkn.crypto.log_output}")
    private String logOutput;
    private String fullLogOut;
    private byte[] key;
    private static final Logger log = LoggerFactory.getLogger(Decrypt.class);
    private Rules rules;
    private Path pathOut;

    @PostConstruct
    private void postConstruct() {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".postConstruct ");
        try {
            fullLogOut = logOutput.trim().replace("#", "");
            pathOut = Paths.get(fullLogOut);
        } catch (Exception e) {
            log.error("No log-output supplied");
        }
        rules = new Rules();
        String uriKey = keyURL.trim().replace("#", "");
        try {
            String b64Key = new String(Files.readAllBytes(Paths.get(uriKey)), "utf-8");
            key = Base64Utils.decodeFromString(b64Key);
        } catch (IOException e) {
            log.error("Error reading key with message: {}", e.getMessage());
        }
    }

    public String decrypt(String b64Source) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".decrypt ");
        byte[] source = Base64Utils.decodeFromString(b64Source);
        List<byte[]> listDecoded = rules.decryptChain(source, key);
        if (listDecoded == null || listDecoded.isEmpty()) {
            return null;
        }
        byte[] originByteArray = listDecoded.get(0);
        String realSource = Base64Utils.encodeToString(originByteArray);
        appendToLog(b64Source, realSource);
        return realSource;
    }

    private void appendToLog(String cipher, String clear) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".appendToLog ");
        String endMarker = "-------------------------------------------------------------";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Clear", clear);
        jsonObject.put("Cipher", cipher);
        try {
            Files.write(pathOut, jsonObject.toString().getBytes(), StandardOpenOption.APPEND);
            Files.write(pathOut, endMarker.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            log.error("Could not write to log with clear and cipher values: {}", e.getMessage());
        }
    }


}
