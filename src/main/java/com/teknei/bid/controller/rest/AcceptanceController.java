package com.teknei.bid.controller.rest;

import com.teknei.bid.dto.AcceptancePerCustomerDTO;
import com.teknei.bid.dto.OperationIdDTO;
import com.teknei.bid.persistence.entities.BidClieFirm;
import com.teknei.bid.persistence.entities.BidClieRegEsta;
import com.teknei.bid.persistence.entities.BidEstaProc;
import com.teknei.bid.persistence.entities.BidTipoFirm;
import com.teknei.bid.persistence.repository.BidClieFirmRepository;
import com.teknei.bid.persistence.repository.BidClieRegEstaRepository;
import com.teknei.bid.persistence.repository.BidEstaProcRepository;
import com.teknei.bid.persistence.repository.BidTipoFirmRepository;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/acceptance")
public class AcceptanceController {

    @Autowired
    private BidTipoFirmRepository tipoFirmRepository;
    @Autowired
    private BidClieFirmRepository bidClieFirmRepository;
    private List<BidTipoFirm> tipoFirmList;
    @Autowired
    private BidEstaProcRepository bidEstaProcRepository;
    @Autowired
    private BidClieRegEstaRepository bidClieRegEstaRepository;
    private static final String STATUS_ACCEPTANCE_MADE = "ACE-TC";
    private static final Logger log = LoggerFactory.getLogger(AcceptanceController.class);

    @PostConstruct
    private void init() {
        initTipoFirmList();
    }

    private void initTipoFirmList() {System.out.println("lblancas:: "+this.getClass().getName()+". ");
        tipoFirmList = tipoFirmRepository.findAll();
    }

    @RequestMapping(value = "/acceptancePerCustomer", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> updateAcceptancePerCustomer(@RequestBody List<AcceptancePerCustomerDTO> dtoList) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".updateAcceptancePerCustomer ");
        JSONObject jsonObject = new JSONObject();
        try {
            dtoList.forEach(c -> updateAcceptancePerCustomer(c));
            jsonObject.put("status", "OK");
            updateStatus(dtoList.get(0).getIdClie(), STATUS_ACCEPTANCE_MADE, dtoList.get(0).getUsernameRequesting());
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error updating acceptance for: {} with message: {}", dtoList, e.getMessage());
            jsonObject.put("status", "ERROR");
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @RequestMapping(value = "/acceptancePerCustomer", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AcceptancePerCustomerDTO>> getAcceptancesPerCustomer(@RequestBody OperationIdDTO operationIdDTO) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getAcceptancesPerCustomer ");
        try {
            List<BidClieFirm> firmsPerCustomer = findClieFirms(operationIdDTO.getOperationId());
            List<AcceptancePerCustomerDTO> acceptancePerCustomerDTOList = pupulateCustomerRelated(firmsPerCustomer, "bid-api");
            return new ResponseEntity<>(acceptancePerCustomerDTOList, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error finding sign requirements per customer with message: {}", e.getMessage());
            return new ResponseEntity<>((List<AcceptancePerCustomerDTO>) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    private void updateAcceptancePerCustomer(AcceptancePerCustomerDTO acceptance) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".updateAcceptancePerCustomer ");
        Long idTipoFirm = find(acceptance.getSignCode()).getIdTipoFirm();
        BidClieFirm clieFirm = bidClieFirmRepository.findByIdClieAndIdTipoFirm(acceptance.getIdClie(), idTipoFirm);
        clieFirm.setBolAcep(acceptance.getAccepted());
        clieFirm.setUsrModi(acceptance.getUsernameRequesting());
        clieFirm.setUsrOpeModi(acceptance.getUsernameRequesting());
        clieFirm.setFchModi(new Timestamp(System.currentTimeMillis()));
        bidClieFirmRepository.save(clieFirm);
    }

    private List<BidClieFirm> findClieFirms(Long idClie) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+". ");
        return bidClieFirmRepository.findAllByIdClieAndIdEsta(idClie, 1);
    }

    private List<AcceptancePerCustomerDTO> pupulateCustomerRelated(List<BidClieFirm> clieFirms, String usernameRequesting) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".pupulateCustomerRelated ");
        List<AcceptancePerCustomerDTO> list =
                clieFirms.stream()
                        .filter(c -> c.getIdEsta() == 1)
                        .map(c2 -> new AcceptancePerCustomerDTO(find(c2.getIdTipoFirm()).getCodTipoFirm(), find(c2.getIdTipoFirm()).getDescTipoFirm(), c2.getBolObli(), c2.getBolAcep(), c2.getIdClie(), usernameRequesting))
                        .collect(Collectors.toList());
        return list;
    }

    private BidTipoFirm find(Long idTipoFirm) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".BidTipoFirm ");
        List<BidTipoFirm> list = tipoFirmList.stream().filter(c -> c.getIdTipoFirm() == idTipoFirm).collect(Collectors.toList());
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    private BidTipoFirm find(String code) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".BidTipoFirm ");
        List<BidTipoFirm> list = tipoFirmList.stream().filter(c -> c.getCodTipoFirm().equals(code)).collect(Collectors.toList());
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    private void updateStatus(Long idClient, String status, String username) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".updateStatus ");
        try {
            BidEstaProc estaProc = bidEstaProcRepository.findTopByCodEstaProcAndIdEsta(status, 1);
            BidClieRegEsta regEsta = bidClieRegEstaRepository.findByIdClieAndIdEstaProc(idClient, estaProc.getIdEstaProc());
            if (regEsta == null) {
                log.warn("Status for process: {} found null", idClient);
            }
            regEsta.setEstaConf(true);
            regEsta.setFchModi(new Timestamp(System.currentTimeMillis()));
            regEsta.setUsrModi(username);
            regEsta.setUsrOpeModi(username);
            bidClieRegEstaRepository.save(regEsta);
        } catch (Exception e) {
            log.error("Error finding status of process for customer: {} with message: {}", idClient, e.getMessage());
        }
    }

}
