FROM openjdk:8-jre-slim
RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt-get -y install \
libreoffice-common \ 
unoconv \
hyphen-af hyphen-en-us \
fonts-dejavu fonts-dejavu-core fonts-dejavu-extra fonts-droid-fallback fonts-dustin fonts-f500 fonts-fanwood fonts-freefont-ttf fonts-liberation \
fonts-lmodern fonts-lyx fonts-sil-gentium fonts-texgyre fonts-tlwg-purisa fonts-opensymbol python wget tzdata python-pip
RUN cp /usr/share/zoneinfo/America/Mexico_City /etc/localtime
RUN echo "America/Mexico_City" > /etc/timezone
RUN pip install Pillow
RUN mkdir -p /opt/bidserver/logs
RUN mkdir -p /home/contracts/odt/templates/
RUN mkdir -p /home/contracts/odt/PDF
COPY target/tkn-service-contracts-1.0.0.jar /home/tkn-service-contracts.jar
COPY contract_generator.py /home/contracts/odt/templates/contract_generator.py
COPY contract_generator_with_cert_ts.py /home/contracts/odt/templates/contract_generator_with_cert_ts.py
COPY contract_generator_sabadell_v1.py /home/contracts/odt/templates/contract_generator_sabadell_v1.py
COPY contract_generator_sabadell_v2.py /home/contracts/odt/templates/contract_generator_sabadell_v2.py
COPY script_init.sh /home/script_init.sh
copy huellaC.jpg /home/huellaC.jpg
COPY plantilla.odt /home/contracts/odt/templates/plantilla.odt
COPY plantilla2.odt /home/contracts/odt/templates/plantilla2.odt
COPY plantilla_sabadell.odt /home/contracts/odt/templates/plantilla_sabadell.odt
COPY FRABK.TTF /usr/share/fonts/truetype/frabk.ttf
COPY FRABKIT.TTF /usr/share/fonts/truetype/frabkit.ttf
COPY franklin-gothic-medium.ttf /usr/share/fonts/truetype/franklin-gothic-medium.ttf
COPY sign.tar.gz /home/contracts/sign.tar.gz
RUN chmod 777 /home/script_init.sh
RUN cd /home/contracts
RUN tar -xzvf /home/contracts/sign.tar.gz -C /home/contracts/
RUN rm /home/contracts/sign.tar.gz
RUN mkdir -p /home/psc
COPY generate_cert_step_01.py /home/psc/generate_cert_step_01.py
COPY SSL.tar.gz /home/psc/SSL.tar.gz
COPY tkn-gen-cert.jar /home/psc/tkn-gen-cert.jar
COPY tkn-sign-doc.jar /home/psc/tkn-sign-doc.jar
COPY tkn-nom151.jar /home/psc/tkn-nom151.jar
COPY image_color_replace.py /home/psc/image_color_replace.py
COPY firma.wsdl /home/psc/firma.wsdl
COPY nom151.wsdl /home/psc/nom151.wsdl
COPY script_add_cert_edicom.sh /home/psc/script_add_cert_edicom.sh
RUN chmod +x /home/psc/script_add_cert_edicom.sh
RUN sh /home/psc/script_add_cert_edicom.sh
COPY cacerts /usr/lib/jvm/java-8-openjdk-amd64/jre/lib/security/
COPY jssecacerts /usr/lib/jvm/java-8-openjdk-amd64/jre/lib/security/
COPY CERT_LOGIN_TEST.tar.gz /home/psc/CERT_LOGIN_TEST.tar.gz
ENTRYPOINT ["/home/script_init.sh"]

